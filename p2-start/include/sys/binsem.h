#ifndef BINSEM_H
#define BINSEM_H
#include <os/sched.h>
#define BINSEM_OP_LOCK 0 // mutex acquire
#define BINSEM_OP_UNLOCK 1 // mutex release
//#define BIN_LOCKED 1
//#define BIN_UNLOCKED 0
typedef enum{
  BIN_LOCKED,
  BIN_UNLOCKED
}bin_status;
typedef struct bin_node{
  int key;
  list_node_t list;
  bin_status status;
}bin;
bin binsignals[100];
int binsemget(int key);
int binsemop(int binsem_id, int op);

#endif // BINSEM_H
