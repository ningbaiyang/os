/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * *
 *            Copyright (C) 2018 Institute of Computing Technology, CAS
 *               Author : Han Shukai (email : hanshukai@ict.ac.cn)
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * *
 *         The kernel's entry, where most of the initialization work is done.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * */

#include <common.h>
#include <os/irq.h>
#include <os/mm.h>
#include <os/sched.h>
#include <screen.h>
#include <sbi.h>
#include <stdio.h>
#include <os/time.h>
#include <os/syscall.h>
#include <os/futex.h>
#include <test.h>
#include<sys/binsem.h>
#include <csr.h>


extern void ret_from_exception();
extern void printk_task1(void);
extern void __global_pointer$();

static void init_pcb_stack(
    ptr_t kernel_stack, ptr_t user_stack, ptr_t entry_point,
    pcb_t *pcb)
{
    regs_context_t *pt_regs =
        (regs_context_t *)(kernel_stack - sizeof(regs_context_t));
    
    /* TODO: initialization registers
     * note: sp, gp, ra, sepc, sstatus
     * gp should be __global_pointer$
     * To run the task in user mode,
     * you should set corresponding bits of sstatus(SPP, SPIE, etc.).
     */
     for(int i=5;i<32;i++)
       pt_regs->regs[i]=0;
     pt_regs->regs[0]=0;
     pt_regs->regs[1]=entry_point;
     pt_regs->regs[2]=user_stack;
     pt_regs->regs[3]=__global_pointer$;
     pt_regs->regs[4]=(uint64_t)pcb;
     pt_regs->sstatus=0x20;
     pt_regs->sepc=entry_point;
     pt_regs->sbadaddr=0;
     pt_regs->scause=0;    

    // set sp to simulate return from switch_to
    /* TODO: you should prepare a stack, and push some values to
     * simulate a pcb context.
     */
    switchto_context_t *switchto_regs =
    	(switchto_context_t *)(kernel_stack - sizeof(regs_context_t)-sizeof(switchto_context_t));
    switchto_regs->regs[0] = ret_from_exception;
    switchto_regs->regs[1] = user_stack;
    for(int i=2;i<14;i++)
       switchto_regs->regs[i]=0;
}

static void init_pcb()
{
     /* initialize all of your pcb and add them into ready_queue
     * TODO:
     */
    for(int i=0;i<8;i++){    
    	pcb[i].kernel_sp = allocPage(1)+PAGE_SIZE;
    	pcb[i].user_sp = allocPage(1)+PAGE_SIZE;
      pcb[i].pid=i;
      if(i<2)
        init_pcb_stack(pcb[i].kernel_sp,pcb[i].user_sp,timer_tasks[i]->entry_point,pcb+i);
      else if(i<6)
        init_pcb_stack(pcb[i].kernel_sp,pcb[i].user_sp,sched2_tasks[i-2]->entry_point,pcb+i);
      else
        init_pcb_stack(pcb[i].kernel_sp,pcb[i].user_sp,lock2_tasks[i-6]->entry_point,pcb+i);
     /* if(i<3)
    	  init_pcb_stack(pcb[i].kernel_sp,pcb[i].user_sp,sched1_tasks[i]->entry_point,pcb+i);
      else if(i<5)
     	  init_pcb_stack(pcb[i].kernel_sp,pcb[i].user_sp,lock_tasks[i-3]->entry_point,pcb+i);   
      else if(i<7)
        init_pcb_stack(pcb[i].kernel_sp,pcb[i].user_sp,timer_tasks[i-5]->entry_point,pcb+i);
      else if(i<11)
        init_pcb_stack(pcb[i].kernel_sp,pcb[i].user_sp,sched2_tasks[i-7]->entry_point,pcb+i);
      else
        init_pcb_stack(pcb[i].kernel_sp,pcb[i].user_sp,lock2_tasks[i-11]->entry_point,pcb+i);*/
    	pcb[i].preempt_count = 0;
    	list_add_tail(&(pcb[i].list), &ready_queue);
    	pcb[i].type = KERNEL_THREAD;
    	pcb[i].status = TASK_READY; 
	}
//	pcb_t *pcb
    /* remember to initialize `current_running`
     * TODO:
     */
    current_running = &pid0_pcb;
}

static void init_syscall(void)
{
    // initialize system call table.
    syscall[SYSCALL_SLEEP]=do_sleep;
    syscall[SYSCALL_FUTEX_WAIT]=futex_wait;
    syscall[SYSCALL_FUTEX_WAKEUP]=futex_wakeup;
    syscall[SYSCALL_WRITE]=screen_write;
    //syscall[SYSCALL_READ]=
    syscall[SYSCALL_CURSOR]=screen_move_cursor;
    syscall[SYSCALL_REFLUSH]=screen_reflush;
    syscall[SYSCALL_GET_TIMEBASE]=get_time_base;
    syscall[SYSCALL_GET_TICK]=get_ticks;
    syscall[SYSCALL_BINSEMGET]=binsemget;
    syscall[SYSCALL_BINSEMOP]=binsemop;
}
// jump from bootloader.
// The beginning of everything >_< ~~~~~~~~~~~~~~
int main()
{
    // init Process Control Block (-_-!)
    for(int i=0;i<100;i++){
      binsignals[i].key=i;
      binsignals[i].status=BIN_UNLOCKED;
      init_list_head(&(binsignals[i].list));
    }
    init_pcb();
    printk("> [INIT] PCB initialization succeeded.\n\r");

    // read CPU frequency
    time_base = sbi_read_fdt(TIMEBASE);
	
    // init futex mechanism
    init_system_futex();

    // init interrupt (^_^)
    init_exception();
    printk("> [INIT] Interrupt processing initialization succeeded.\n\r");

    // init system call table (0_0)
    init_syscall();
    printk("> [INIT] System call initialized successfully.\n\r");

    // fdt_print(riscv_dtb);

    // init screen (QAQ)
    init_screen();
    printk("> [INIT] SCREEN initialization succeeded.\n\r");

    // TODO:
    // Setup timer interrupt and enable all interrupt
    uint64_t time=get_ticks();
    // note: use sbi_set_timer
    sbi_set_timer(time+80000);
    //reset_irq_timer();
    while (1) {
        // (QAQQQQQQQQQQQ)
        // If you do non-preemptive scheduling, you need to use it
        // to surrender control do_scheduler();
        // enable_interrupt();
        // __asm__ __volatile__("wfi\n\r":::);
        do_scheduler();
    };
    return 0;
}
