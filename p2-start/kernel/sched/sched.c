#include <os/list.h>
#include <os/mm.h>
#include <os/lock.h>
#include <os/sched.h>
#include <os/time.h>
#include <os/irq.h>
#include <screen.h>
#include <stdio.h>
#include <assert.h>
#include<sys/binsem.h>

pcb_t pcb[NUM_MAX_TASK];
const ptr_t pid0_stack = INIT_KERNEL_STACK + PAGE_SIZE;
pcb_t pid0_pcb = {
    .pid = 0,
    .kernel_sp = (ptr_t)pid0_stack,
    .user_sp = (ptr_t)pid0_stack,
    .preempt_count = 0
};

 LIST_HEAD(ready_queue);
 LIST_HEAD(sleep_queue);

/* current running task PCB */
pcb_t * volatile current_running;
pcb_t * volatile pre_running;
/* global process id */
pid_t process_id = 1;


//bin binsignals[100];
void do_scheduler(void)
{
    // TODO schedule
    // Modify the current_running pointer.
    timer_check();
    pre_running=current_running;
	  current_running = list_entry(ready_queue.next, pcb_t, list);//pop(&ready_queue)-3*sizeof(reg_t);
    //if(pre_running!=&pid0_pcb)
      //list_add_tail(&(pre_running->list), &ready_queue);
    list_del(ready_queue.next);
    if(pre_running->status!=TASK_BLOCKED)
    {
      pre_running->status=TASK_READY;
      if(pre_running!=&pid0_pcb){
        list_add_tail(&(pre_running->list), &ready_queue);
      }
    }
    // restore the current_runnint's cursor_x and cursor_y
    vt100_move_cursor(current_running->cursor_x,
                      current_running->cursor_y);
    screen_cursor_x = current_running->cursor_x;
    screen_cursor_y = current_running->cursor_y;
    // TODO: switch_to current_running
    switch_to(pre_running,current_running);
}

void do_sleep(uint32_t sleep_time)
{
    // TODO: sleep(seconds)
    // note: you can assume: 1 second = `timebase` ticks
    // 1. block the current_running
    list_del(&(current_running->list));
    list_add_tail(&(current_running->list), &sleep_queue);
    current_running->status=TASK_BLOCKED;
    // 2. create a timer which calls `do_unblock` when timeout
    //timer_create(do_unblock, &(current_running->list), sleep_time);
    // 3. reschedule because the current_running is blocked.
    timer_create(&do_unblock,&current_running->list,(uint64_t)sleep_time);
    do_scheduler();
}

void do_block(list_node_t *pcb_node, list_head *queue)
{
    // TODO: block the pcb task into the block queue
  
  list_del(pcb_node);
	list_add_tail(pcb_node, queue);
	//*(sizeof(pid_t)+sizeof(task_type_t)+pcb_node)=TASK_BLOCKED; 
  (list_entry(pcb_node,pcb_t,list))->status=TASK_BLOCKED;
  if(ready_queue.next!=&ready_queue){
  do_scheduler();
  }
}

void do_unblock(list_node_t *pcb_node)
{
    // TODO: unblock the `pcb` from the block queue
    list_del(pcb_node);
    list_add_tail(pcb_node, &ready_queue);
    //*(sizeof(task_type_t)+sizeof(pid_t)+sizeof(list_node_t)+pcb_node)=TASK_READY;
    (list_entry(pcb_node,pcb_t,list))->status=TASK_READY;
}
int binsemget(int key){
  for(int i=0;i<100;i++)
    if(binsignals[i].key==key)
      return i;
  return 0;
}
int binsemop(int binsem_id, int op){
  if(op==BINSEM_OP_LOCK){
    if(binsignals[binsem_id].status==BIN_LOCKED){
     do_block(&(current_running->list), &(binsignals[binsem_id].list));
     //binsignals[binsem_id].status=BIN_LOCKED;
    }
     binsignals[binsem_id].status=BIN_LOCKED;
  }
  else{
    if(!list_empty(&binsignals[binsem_id].list)){
    do_unblock(binsignals[binsem_id].list.next);
    binsignals[binsem_id].status=BIN_LOCKED;
    }
    else binsignals[binsem_id].status=BIN_UNLOCKED;
    //do_scheduler();
  }
}
