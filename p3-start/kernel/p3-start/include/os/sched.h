/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * *
 *            Copyright (C) 2018 Institute of Computing Technology, CAS
 *               Author : Han Shukai (email : hanshukai@ict.ac.cn)
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * *
 *        Process scheduling related content, such as: scheduler, process blocking,
 *                 process wakeup, process creation, process kill, etc.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * */

#ifndef INCLUDE_SCHEDULER_H_
#define INCLUDE_SCHEDULER_H_

#include <type.h>
#include <os/list.h>
#include <os/mm.h>
#include<os/time.h>
#include<os.h>
#include<os/lock.h>
#include<mthread.h>

#define NUM_MAX_TASK 10000

/* used to save register infomation */
typedef struct regs_context
{
    /* Saved main processor registers.*/
    reg_t regs[32];

    /* Saved special registers. */
    reg_t sstatus;
    reg_t sepc;
    reg_t sbadaddr;
    reg_t scause;
} regs_context_t;

/* used to save register infomation in switch_to */
typedef struct switchto_context
{
    /* Callee saved registers.*/
    reg_t regs[14];
} switchto_context_t;

typedef enum {
    TASK_BLOCKED,
    TASK_RUNNING,
    TASK_READY,
    TASK_EXITED,
    TASK_ZOMBIE,
} task_status_t;

/*typedef enum {
    KERNEL_PROCESS,
    KERNEL_THREAD,
    USER_PROCESS,
    USER_THREAD,
} task_type_t;

typedef enum {
    ENTER_ZOMBIE_ON_EXIT,
    AUTO_CLEANUP_ON_EXIT,
} spawn_mode_t;*/


/* Process Control Block */
typedef struct pcb
{
    /* register context */
    // this must be this order!! The order is defined in regs.h
    reg_t kernel_sp;
    reg_t user_sp;


    // count the number of disable_preempt
    // enable_preempt enables CSR_SIE only when preempt_count == 0
    reg_t preempt_count;

    /* previous, next pointer */
    ptr_t kernel_stack_base;
    ptr_t user_stack_base;

    /* previous, next pointer */
    list_node_t list;
    list_head wait_list;

    /* process id */
    pid_t pid;

    /* kernel/user thread/process */
    task_type_t type;

    /* BLOCK | READY | RUNNING */
    task_status_t status;

    /* cursor position */
    int cursor_x;
    int cursor_y;
    timer_t time;
    spawn_mode_t mode;
    mutex_lock_t* mutex_lock;
    pid_t waitpid;
} pcb_t;

/* task information, used to init PCB */
/*typedef struct task_info
{
    ptr_t entry_point;
    task_type_t type;
} task_info_t;*/

/* ready queue to run */
extern list_head ready_queue;
extern list_head sleep_queue;

/* current running task PCB */
extern pcb_t * volatile current_running;
extern pid_t process_id;
extern int pcb_valid[100];

extern pcb_t pcb[NUM_MAX_TASK];
extern pcb_t pid0_pcb;
extern const ptr_t pid0_stack;

static void init_pcb_stack(ptr_t kernel_stack, ptr_t user_stack, ptr_t entry_point,pcb_t *pcb,void* argument);
extern void switch_to(pcb_t *prev, pcb_t *next);
void do_scheduler(void);
void do_sleep(uint32_t);
void do_block(list_node_t *, list_head *queue);
void do_unblock(list_node_t *);
pid_t do_spawn(task_info_t *info, void* arg, spawn_mode_t mode);
void do_exit(void);
int do_wait(pid_t pid);
int do_kill(pid_t pid);
void do_ps(uint32_t time);
pid_t do_getpid();
int do_mthread_barrier_init(mthread_barrier_t * barrier, unsigned count);
int do_mthread_barrier_wait(mthread_barrier_t *barrier);
int do_mthread_barrier_destroy(mthread_barrier_t *barrier);
int do_mthread_cond_init(mthread_cond_t *cond);
int do_mthread_cond_destroy(mthread_cond_t *cond);
int do_mthread_cond_wait(mthread_cond_t *cond, mthread_mutex_t *mutex);
int do_mthread_cond_signal(mthread_cond_t *cond);
int do_mthread_cond_broadcast(mthread_cond_t *cond);
int do_mthread_mutex_init(mthread_mutex_t *lock);
int do_mthread_mutex_destroy(mthread_mutex_t *lock);
int do_mthread_mutex_trylock(mthread_mutex_t *lock);
int do_mthread_mutex_lock(mthread_mutex_t *lock);
int do_mthread_mutex_unlock(mthread_mutex_t *lock);
#endif
