#include <os/time.h>
#include <os/mm.h>
#include <os/irq.h>
#include <type.h>

LIST_HEAD(timers);

uint64_t time_elapsed = 0;
uint32_t time_base = 0;

void timer_create(TimerCallback func, void* parameter, uint64_t tick)
{
    disable_preempt();

    // TODO:
    uint64_t begin=get_timer();
    /*timer_t  mytimer;
    mytimer.timeout_tick=begin+tick;
    mytimer.list=*((list_node_t *)parameter);
    mytimer.callback_func=func;
    mytimer.parameter=parameter;*/
    current_running->time.timeout_tick=begin+tick;
    current_running->time.callback_func=func;
    current_running->time.parameter=parameter;
   // list_add_tail(&(mytimer.list),&timers);
   // (list_entry(parameter,pcb_t,list))->status=TASK_BLOCKED;
    enable_preempt();
}

void timer_check()
{
    disable_preempt();
   // ticks=get_ticks();
    /* TODO: check all timers
     *  if timeouts, call callback_func and free the timer.
     */
    for(list_node_t *now=sleep_queue.next;now!=&sleep_queue;){
      pcb_t* mytimer=(list_entry(now,pcb_t,list));
      if((get_timer())>=mytimer->time.timeout_tick){
      //(list_entry(now,time_t,list))
      now=now->next;
        mytimer->time.callback_func(mytimer->time.parameter);
        //list_node_t * to_delete=now;
        //now=now->next;
        //list_del(to_delete);
        //list_add_tail(to_delete,&ready_queue);
        //(list_entry(to_delete,pcb_t,list))->status=TASK_READY;
      }
      else
        now=now->next;
    }
    enable_preempt();
}

uint64_t get_ticks()
{
    __asm__ __volatile__(
        "rdtime %0"
        : "=r"(time_elapsed));
    return time_elapsed;
}

uint64_t get_timer()
{
    return get_ticks() / time_base;
}

uint64_t get_time_base()
{
    return time_base;
}

void latency(uint64_t time)
{
    uint64_t begin_time = get_timer();

    while (get_timer() - begin_time < time);
    return;
}
