#include <sys/syscall.h>
#include <stdint.h>
#include <common.h>
#include <os/irq.h>
#include <os/mm.h>
#include <os/sched.h>
#include <screen.h>
#include <sbi.h>
#include <stdio.h>
#include <os/time.h>
#include <os/syscall.h>
#include <os/futex.h>
#include <test.h>
#include<sys/binsem.h>
#include <csr.h>
#include<mailbox.h>
#include<mthread.h>
int sys_mthread_mutex_init(mthread_mutex_t *lock){
  return invoke_syscall(SYSCALL_MTHREAD_MUTEX_INIT, lock, IGNORE, IGNORE);
}
int sys_mthread_mutex_destroy(mthread_mutex_t *lock){
  return invoke_syscall(SYSCALL_MTHREAD_MUTEX_DESTROY, lock, IGNORE, IGNORE);
}
int sys_mthread_mutex_trylock(mthread_mutex_t *lock){
  return invoke_syscall(SYSCALL_MTHREAD_MUTEX_TRYLOCK, lock, IGNORE, IGNORE);
}
int sys_mthread_mutex_lock(mthread_mutex_t *lock){
  return invoke_syscall(SYSCALL_MTHREAD_MUTEX_LOCK, lock, IGNORE, IGNORE);
}
int sys_mthread_mutex_unlock(mthread_mutex_t *lock){
  return invoke_syscall(SYSCALL_MTHREAD_MUTEX_UNLOCK, lock, IGNORE, IGNORE);
}
int sys_mthread_barrier_init(mthread_barrier_t * barrier, unsigned count){
	return invoke_syscall(SYSCALL_MTHREAD_BARRIER_INIT, barrier, count, IGNORE);
}
int sys_mthread_barrier_wait(mthread_barrier_t *barrier){
	return invoke_syscall(SYSCALL_MTHREAD_BARRIER_WAIT, barrier, IGNORE, IGNORE);
}
int sys_mthread_barrier_destroy(mthread_barrier_t *barrier){
	return invoke_syscall(SYSCALL_MTHREAD_BARRIER_DESTROY, barrier, IGNORE, IGNORE);
}
int sys_mthread_cond_init(mthread_cond_t *cond){
	return invoke_syscall(SYSCALL_MTHREAD_COND_INIT, cond, IGNORE, IGNORE);
}
int sys_mthread_cond_destroy(mthread_cond_t *cond) {
	return invoke_syscall(SYSCALL_MTHREAD_COND_DESTROY, cond, IGNORE, IGNORE);
}
int sys_mthread_cond_wait(mthread_cond_t *cond, mthread_mutex_t *mutex)
{
    return invoke_syscall(SYSCALL_MTHREAD_COND_WAIT, cond, mutex, IGNORE);

}
int sys_mthread_cond_signal(mthread_cond_t *cond)
{
	return invoke_syscall(SYSCALL_MTHREAD_COND_SIGNAL, cond, IGNORE, IGNORE);

}
int sys_mthread_cond_broadcast(mthread_cond_t *cond)
{
   return invoke_syscall(SYSCALL_MTHREAD_COND_BROADCAST, cond, IGNORE, IGNORE);
}
mailbox_t sys_mbox_open(char *name)
{
	return invoke_syscall(SYSCALL_MBOX_OPEN, name, IGNORE, IGNORE);
}
void sys_mbox_close(mailbox_t mailbox){
	invoke_syscall(SYSCALL_MBOX_CLOSE, mailbox, IGNORE, IGNORE);
}
void sys_mbox_send(mailbox_t mailbox, void *msg, int msg_length){
	invoke_syscall(SYSCALL_MBOX_SEND, mailbox, msg, msg_length);
}
void sys_mbox_recv(mailbox_t mailbox, void *msg, int msg_length){
	invoke_syscall(SYSCALL_MBOX_RECV, mailbox, msg, msg_length);
}
pid_t sys_spawn(task_info_t *info, void* arg, spawn_mode_t mode)
{	
	return invoke_syscall(SYSCALL_SPAWN, info, arg, mode);

}
void sys_exit(void)
{
	invoke_syscall(SYSCALL_EXIT, IGNORE, IGNORE, IGNORE);

}
int sys_kill(pid_t pid)
{
	return invoke_syscall(SYSCALL_KILL, pid, IGNORE, IGNORE);

}
int sys_waitpid(pid_t pid)
{
	return invoke_syscall(SYSCALL_WAIT, pid, IGNORE, IGNORE);	
}
pid_t sys_getpid(){
  return invoke_syscall(SYSCALL_GETPID, IGNORE, IGNORE, IGNORE);
}
void sys_ps(uint32_t time)
{
    invoke_syscall(SYSCALL_PS, time, IGNORE, IGNORE);
}
void sys_clear(){
	invoke_syscall(SYSCALL_SCREEN_CLEAR,IGNORE, IGNORE, IGNORE);
}
/*void sys_yield()
{
    invoke_syscall(SYSCALL_YIELD, time, IGNORE, IGNORE);
}*/
void sys_sleep(uint32_t time)
{
    invoke_syscall(SYSCALL_SLEEP, time, IGNORE, IGNORE);
}

void sys_write(char *buff)
{
    invoke_syscall(SYSCALL_WRITE, (uintptr_t)buff, IGNORE, IGNORE);
}

void sys_reflush()
{
    invoke_syscall(SYSCALL_REFLUSH, IGNORE, IGNORE, IGNORE);
}
void sys_screen_clear()
{
    invoke_syscall(SYSCALL_SCREEN_CLEAR, IGNORE, IGNORE, IGNORE);
}
void sys_screen_write_ch(in){
    invoke_syscall(SYSCALL_SCREEN_WRITE_CH, IGNORE, IGNORE, IGNORE);
}
void sys_move_cursor(int x, int y)
{
    invoke_syscall(SYSCALL_CURSOR, x, y, IGNORE);
}

void sys_futex_wait(volatile uint64_t *val_addr, uint64_t val)
{
    invoke_syscall(SYSCALL_FUTEX_WAIT, (uintptr_t)val_addr, val, IGNORE);
}

void sys_futex_wakeup(volatile uint64_t *val_addr, int num_wakeup)
{
    invoke_syscall(SYSCALL_FUTEX_WAKEUP, (uintptr_t)val_addr, num_wakeup, IGNORE);
}

long sys_get_timebase()
{
    return invoke_syscall(SYSCALL_GET_TIMEBASE, IGNORE, IGNORE, IGNORE);
}

long sys_get_tick()
{
    return invoke_syscall(SYSCALL_GET_TICK, IGNORE, IGNORE, IGNORE);
}

long sys_binsemget(int k)
{
    return invoke_syscall(SYSCALL_BINSEMGET,k,IGNORE,IGNORE);
}

long sys_binsemop(int binsem_id, int op)
{
    return invoke_syscall(SYSCALL_BINSEMOP,binsem_id,op,IGNORE);
}
int sys_get_char(){
	return invoke_syscall(SYSCALL_GET_CHAR,IGNORE, IGNORE, IGNORE);
} 

