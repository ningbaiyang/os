#ifndef PGTABLE_H
#define PGTABLE_H

#include <type.h>
#include <sbi.h>

#define SATP_MODE_SV39 8
#define SATP_MODE_SV48 9

#define SATP_ASID_SHIFT 44lu
#define SATP_MODE_SHIFT 60lu

#define NORMAL_PAGE_SHIFT 12lu
#define NORMAL_PAGE_SIZE (1lu << NORMAL_PAGE_SHIFT)
#define LARGE_PAGE_SHIFT 21lu
#define LARGE_PAGE_SIZE (1lu << NORMAL_PAGE_SHIFT)

/*
 * Flush entire local TLB.  'sfence.vma' implicitly fences with the instruction
 * cache as well, so a 'fence.i' is not necessary.
 */
static inline void local_flush_tlb_all(void)
{
    __asm__ __volatile__ ("sfence.vma" : : : "memory");
}

/* Flush one page from local TLB */
static inline void local_flush_tlb_page(unsigned long addr)
{
    __asm__ __volatile__ ("sfence.vma %0" : : "r" (addr) : "memory");
}

static inline void local_flush_icache_all(void)
{
    asm volatile ("fence.i" ::: "memory");
}

static inline void flush_icache_all(void)
{
    local_flush_icache_all();
    sbi_remote_fence_i(NULL);
}

static inline void flush_tlb_all(void)
{
    local_flush_tlb_all();
    sbi_remote_sfence_vma(NULL, 0, -1);
}
static inline void flush_tlb_page_all(unsigned long addr)
{
    local_flush_tlb_page(addr);
    sbi_remote_sfence_vma(NULL, 0, -1);
}

static inline void set_satp(
    unsigned mode, unsigned asid, unsigned long ppn)
{
    unsigned long __v =
        (unsigned long)(((unsigned long)mode << SATP_MODE_SHIFT) | ((unsigned long)asid << SATP_ASID_SHIFT) | ppn);
    __asm__ __volatile__("sfence.vma\ncsrw satp, %0" : : "rK"(__v) : "memory");
}

#define PGDIR_PA 0x5e000000lu  // use bootblock's page as PGDIR

/*
 * PTE format:
 * | XLEN-1  10 | 9             8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0
 *       PFN      reserved for SW   D   A   G   U   X   W   R   V
 */

#define _PAGE_ACCESSED_OFFSET 6

#define _PAGE_PRESENT (1 << 0)
#define _PAGE_READ (1 << 1)     /* Readable */
#define _PAGE_WRITE (1 << 2)    /* Writable */
#define _PAGE_EXEC (1 << 3)     /* Executable */
#define _PAGE_USER (1 << 4)     /* User */
#define _PAGE_GLOBAL (1 << 5)   /* Global */
#define _PAGE_ACCESSED (1 << 6) /* Set by hardware on any access \
                                 */
#define _PAGE_DIRTY (1 << 7)    /* Set by hardware on any write */
#define _PAGE_SOFT (1 << 8)     /* Reserved for software */

#define _PAGE_PFN_SHIFT 10lu

#define VA_MASK ((1lu << 39) - 1)

#define PPN_BITS 9lu
#define NUM_PTE_ENTRY (1 << PPN_BITS)
#define PA_SHIFT 21lu

#define _PAGE_SIZE 2097152 //2MB
#define PAGE_START 0x50000000
#define PAGE_END   0x60000000
#define PAGE_NUM   128
#define PAGE_SIZE  4096

typedef uint64_t PTE;

static inline uintptr_t kva2pa(uintptr_t kva)
{
    // TODO:
<<<<<<< HEAD
=======
    return kva - 0xffffffc000000000;
>>>>>>> ef4424e744cd4abd87e3460685865c472dd7be7b
}

static inline uintptr_t pa2kva(uintptr_t pa)
{
    // TODO:
<<<<<<< HEAD
}

static inline uint64_t get_pa(PTE entry)
{
    // TODO:
    /*long pfn;
    pfn = get_pfn(PTE entry);
    uint64_t ppn = (uint64_t)pfn>>PPN_BITS;
    ppn = ppn<<PA_SHIFT;
    return ppn;*/

   // return page_frame_used[pfn]->paddr;
=======
    return pa + 0xffffffc000000000;
}
static inline long get_pfn(PTE entry)
{
    // TODO:
    //long pfn;
    return (entry >> _PAGE_PFN_SHIFT);
    //return pfn;
}
static inline uint64_t get_pa(PTE entry)
{
    // TODO:
    long pfn;
    pfn = get_pfn(entry);


   // return page_frame_used[pfn]->paddr;
   return pfn<< NORMAL_PAGE_SHIFT;
>>>>>>> ef4424e744cd4abd87e3460685865c472dd7be7b
}

static inline uintptr_t get_kva_of(uintptr_t va, uintptr_t pgdir_va)
{
    // TODO:
}

/* Get/Set page frame number of the `entry` */
<<<<<<< HEAD
static inline long get_pfn(PTE entry)
{
    // TODO:
    //long pfn;
    return (entry >> _PAGE_PFN_SHIFT);
    //return pfn;
}
static inline void set_pfn(PTE *entry, uint64_t pfn)
{
    // TODO:
    PTE temp = *entry;
    temp=temp << (64-_PAGE_PFN_SHIFT);
    temp=temp >> (64-_PAGE_PFN_SHIFT);
    *entry = ( (pfn << _PAGE_PFN_SHIFT) | (temp) );
=======

static inline void set_pfn(PTE *entry, uint64_t pfn)
{
    // TODO:
    uint64_t rest = *entry & 0x3ff;
    *entry = (pfn << _PAGE_PFN_SHIFT) | rest;
>>>>>>> ef4424e744cd4abd87e3460685865c472dd7be7b
}

/* Get/Set attribute(s) of the `entry` */
static inline long get_attribute(PTE entry, uint64_t mask)
{
    // TODO:
}
static inline void set_attribute(PTE *entry, uint64_t bits)
{
    // TODO:
<<<<<<< HEAD
=======
    uint64_t pfn = (uint64_t)get_pfn(*entry);
    *entry = (pfn << _PAGE_PFN_SHIFT) | bits; 
>>>>>>> ef4424e744cd4abd87e3460685865c472dd7be7b
}

static inline void clear_pgdir(uintptr_t pgdir_addr)
{
    // TODO:
    long i;
    for(i=0;i<=PAGE_SIZE;i+=8){
        long* temp=(long*)pgdir_addr+i;
        *temp=0;
    }
}

#endif  // PGTABLE_H
