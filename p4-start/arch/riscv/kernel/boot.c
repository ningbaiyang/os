/* RISC-V kernel boot stage */
#include <context.h>
#include <os/elf.h>
#include <os/mm.h>
#include <pgtable.h>
#include <sbi.h>

typedef void (*kernel_entry_t)(unsigned long, uintptr_t);

extern unsigned char _elf_main[];
extern unsigned _length_main;
int pages[128];
uintptr_t first_page_table;
int first_page_table_valid[512]; 
<<<<<<< HEAD
uintptr_t memstart=PGDIR_PA;
=======
uintptr_t alloc_start=0x5e000000;
>>>>>>> ef4424e744cd4abd87e3460685865c472dd7be7b
/********* setup memory mapping ***********/
uintptr_t alloc_page(uint64_t size)
{
    // TODO: alloc pages for page table entries
    uintptr_t ret;
<<<<<<< HEAD
    ret = memstart;//ROUND(memCurr, size);
    memstart = ret + size;
=======
    ret = alloc_start;//ROUND(memCurr, size);
    alloc_start = ret + size;
>>>>>>> ef4424e744cd4abd87e3460685865c472dd7be7b
    return ret;
    
}

// using 2MB large page
void map_page(uint64_t va, uint64_t pa, PTE *pgdir)
{
    // TODO: map va to pa
    uint64_t vpn2;
    //uint64_t pa1;
    vpn2=(uint64_t)va>>30;
    vpn2=(uint64_t)(vpn2&0x00000000000001ff);
    PTE ptable,ptable1;
    uintptr_t page_addr;
    if(*(pgdir+vpn2)==0){    
      page_addr=alloc_page(PAGE_SIZE);
      page_addr=page_addr>>2;
      page_addr=page_addr|_PAGE_PRESENT;
    	*(pgdir+vpn2)=(PTE)page_addr;
    /*	ptable=*(pgdir+vpn2);
      ptable=ptable|_PAGE_PRESENT;
      ptable=ptable&(~_PAGE_READ)&(~_PAGE_WRITE)&(~_PAGE_EXEC);
      *(pgdir+vpn2)=ptable;*/
	  }
    ptable1=*(pgdir+vpn2);
    ptable1=ptable1<<2;
    ptable1=(ptable1&0xfffff000);
    uint64_t vpn1;
    vpn1=(uint64_t)va>>21;
    vpn1=(uint64_t)(vpn1&0x00000000000001ff);
    PTE *pte;
    pte=(PTE*)ptable1+vpn1;
    *pte=(PTE)(pa>>2);
    *pte=(PTE)(*pte  | _PAGE_PRESENT);
    *pte=(PTE)(*pte | _PAGE_READ | _PAGE_WRITE | _PAGE_EXEC );
    *pte=(PTE)(*pte   | _PAGE_WRITE | _PAGE_EXEC | _PAGE_ACCESSED | _PAGE_DIRTY);
}

void enable_vm()
{
    // TODO: write satp to enable paging
    set_satp(SATP_MODE_SV39, 0, 0x800000000005e000);
    // remember to flush TLB
    local_flush_tlb_all();
}

/* Sv-39 mode
 * 0x0000_0000_0000_0000-0x0000_003f_ffff_ffff is for user mode
 * 0xffff_ffc0_0000_0000-0xffff_ffff_ffff_ffff is for kernel mode
 */
void setup_vm()
{
    // TODO:
    clear_pgdir(PGDIR_PA);
    // map kernel virtual address(kva) to kernel physical
    first_page_table=alloc_page(PAGE_SIZE);
    uint64_t kpa = 0x50200000;
    //uint64_t kva = kpa + 0xffffffc000000000;
    uint64_t kva = kpa ;
    map_page(kva,kpa, first_page_table);
    uintptr_t pa=0x50000000;
    uintptr_t va;
    uintptr_t step=_PAGE_SIZE;
    for(;pa<=0x60000000;pa+=step){
      //if(pa!=0x50200000){
    	va=pa+0xffffffc000000000;
    	map_page(va,pa, first_page_table);
     // }
<<<<<<< HEAD
	}
=======
	  }
>>>>>>> ef4424e744cd4abd87e3460685865c472dd7be7b
    // address(kpa) kva = kpa + 0xffff_ffc0_0000_0000 use 2MB page,
    // map all physical memory
	
    // enable virtual memory
    enable_vm();
    
}

uintptr_t directmap(uintptr_t kva, uintptr_t pgdir)
{
    // ignore pgdir
    return kva;
}

kernel_entry_t start_kernel = NULL;

/*********** start here **************/
int boot_kernel(unsigned long mhartid, uintptr_t riscv_dtb)
{
    if (mhartid == 0) {
        setup_vm();
        // load kernel
        start_kernel =
            (kernel_entry_t)load_elf(_elf_main, _length_main,
                                     PGDIR_PA, directmap);
    } else {
        // TODO: what should we do for other cores?
        ;
    }
    start_kernel(mhartid, riscv_dtb);
    return 0;
}
