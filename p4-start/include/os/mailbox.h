#ifndef INCLUDE_MAIL_BOX_
#define INCLUDE_MAIL_BOX_

#include <os/mthread.h>
#include <os/list.h>

#define MAX_MBOX_LENGTH (64)

// TODO: please define mailbox_t;
// mailbox_t is just an id of kernel's mail box.
typedef int mailbox_t;
typedef struct mailbox{
    mthread_mutex_t mutex;//atomic
    list_head full;//process blocked because hte mailbox is full
    list_head empty;//process blocked because hte mailbox is empty
    char name[50];
    //char msg[10000];
    int msg[1000];
    int left_space;
    int user;
    int read;
    int write;
   // int front;
    //int rear;//the start and end of the message
    int wait;
    //list_head mbox_queue;

}mbox;
mailbox_t mbox_open(char *);
int mailbox_num;
mbox boxes[10];
void mbox_close(mailbox_t);
void mbox_send(mailbox_t, void *, int);
void mbox_recv(mailbox_t, void *, int);


#endif
