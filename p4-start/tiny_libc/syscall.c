#include <sys/syscall.h>
#include <sys/shm.h>
#include <stdint.h>
#include <common.h>
#include <os/irq.h>
#include <os/mm.h>
#include <os/sched.h>
//#include <drivers/screen.h>
#include <sbi.h>
#include <stdio.h>
#include <os/time.h>
#include <os/syscall.h>
#include <os/futex.h>
#include <test.h>
#include<sys/binsem.h>
#include <csr.h>
#include<mailbox.h>
#include<mthread.h>
int sys_mthread_mutex_init(mthread_mutex_t *lock){
  return invoke_syscall(SYSCALL_MTHREAD_MUTEX_INIT, lock, IGNORE, IGNORE, IGNORE);
}
int sys_mthread_mutex_destroy(mthread_mutex_t *lock){
  return invoke_syscall(SYSCALL_MTHREAD_MUTEX_DESTROY, lock, IGNORE, IGNORE, IGNORE);
}
int sys_mthread_mutex_trylock(mthread_mutex_t *lock){
  return invoke_syscall(SYSCALL_MTHREAD_MUTEX_TRYLOCK, lock, IGNORE, IGNORE, IGNORE);
}
int sys_mthread_mutex_lock(mthread_mutex_t *lock){
  return invoke_syscall(SYSCALL_MTHREAD_MUTEX_LOCK, lock, IGNORE, IGNORE, IGNORE);
}
int sys_mthread_mutex_unlock(mthread_mutex_t *lock){
  return invoke_syscall(SYSCALL_MTHREAD_MUTEX_UNLOCK, lock, IGNORE, IGNORE, IGNORE);
}
int sys_mthread_barrier_init(mthread_barrier_t * barrier, unsigned count){
	return invoke_syscall(SYSCALL_MTHREAD_BARRIER_INIT, barrier, count, IGNORE, IGNORE);
}
int sys_mthread_barrier_wait(mthread_barrier_t *barrier){
	return invoke_syscall(SYSCALL_MTHREAD_BARRIER_WAIT, barrier, IGNORE, IGNORE, IGNORE);
}
int sys_mthread_barrier_destroy(mthread_barrier_t *barrier){
	return invoke_syscall(SYSCALL_MTHREAD_BARRIER_DESTROY, barrier, IGNORE, IGNORE, IGNORE);
}
int sys_mthread_cond_init(mthread_cond_t *cond){
	return invoke_syscall(SYSCALL_MTHREAD_COND_INIT, cond, IGNORE, IGNORE, IGNORE);
}
int sys_mthread_cond_destroy(mthread_cond_t *cond) {
	return invoke_syscall(SYSCALL_MTHREAD_COND_DESTROY, cond, IGNORE, IGNORE, IGNORE);
}
int sys_mthread_cond_wait(mthread_cond_t *cond, mthread_mutex_t *mutex)
{
    return invoke_syscall(SYSCALL_MTHREAD_COND_WAIT, cond, mutex, IGNORE, IGNORE);

}
int sys_mthread_cond_signal(mthread_cond_t *cond)
{
	return invoke_syscall(SYSCALL_MTHREAD_COND_SIGNAL, cond, IGNORE, IGNORE, IGNORE);

}
int sys_mthread_cond_broadcast(mthread_cond_t *cond)
{
   return invoke_syscall(SYSCALL_MTHREAD_COND_BROADCAST, cond, IGNORE, IGNORE, IGNORE);
}
mailbox_t sys_mbox_open(char *name)
{
	return invoke_syscall(SYSCALL_MBOX_OPEN, name, IGNORE, IGNORE, IGNORE);
}
void sys_mbox_close(mailbox_t mailbox){
	invoke_syscall(SYSCALL_MBOX_CLOSE, mailbox, IGNORE, IGNORE, IGNORE);
}
void sys_mbox_send(mailbox_t mailbox, void *msg, int msg_length){
	invoke_syscall(SYSCALL_MBOX_SEND, mailbox, msg, msg_length, IGNORE);
}
void sys_mbox_recv(mailbox_t mailbox, void *msg, int msg_length){
	invoke_syscall(SYSCALL_MBOX_RECV, mailbox, msg, msg_length, IGNORE);
}
pid_t sys_spawn(uintptr_t info, void* arg, spawn_mode_t mode)
{
    return invoke_syscall(SYSCALL_SPAWN, (uintptr_t)info,
                          (uintptr_t) arg, mode, IGNORE);
}
void sys_ps(uint32_t time)
{
    invoke_syscall(SYSCALL_PS, time, IGNORE, IGNORE, IGNORE);
}
void sys_clear(){
	invoke_syscall(SYSCALL_SCREEN_CLEAR,IGNORE, IGNORE, IGNORE, IGNORE);
}
void sys_exit(void)
{
    invoke_syscall(SYSCALL_EXIT, IGNORE, IGNORE, IGNORE, IGNORE);
}

void sys_sleep(uint32_t time)
{
    invoke_syscall(SYSCALL_SLEEP, time, IGNORE, IGNORE, IGNORE);
}

int sys_kill(pid_t pid)
{
    return invoke_syscall(SYSCALL_KILL, pid, IGNORE, IGNORE, IGNORE);
}

int sys_waitpid(pid_t pid)
{
    return invoke_syscall(SYSCALL_WAITPID, pid, IGNORE, IGNORE, IGNORE);
}

pid_t sys_exec(const char *file_name, int argc, char* argv[], spawn_mode_t mode)
{
    return invoke_syscall(SYSCALL_EXEC, (uintptr_t)file_name, argc, (uintptr_t)argv, mode);
}

void sys_show_exec()
{
    invoke_syscall(SYSCALL_SHOW_EXEC, IGNORE, IGNORE, IGNORE, IGNORE);
}

void sys_write(char *buff)
{
    invoke_syscall(SYSCALL_WRITE, (uintptr_t)buff, IGNORE, IGNORE, IGNORE);
}

void sys_reflush()
{
    invoke_syscall(SYSCALL_REFLUSH, IGNORE, IGNORE, IGNORE, IGNORE);
}

void sys_move_cursor(int x, int y)
{
    invoke_syscall(SYSCALL_CURSOR, x, y, IGNORE, IGNORE);
}

void sys_futex_wait(volatile uint64_t *val_addr, uint64_t val)
{
    invoke_syscall(SYSCALL_FUTEX_WAIT, (uintptr_t)val_addr, val, IGNORE, IGNORE);
}

void sys_futex_wakeup(volatile uint64_t *val_addr, int num_wakeup)
{
    invoke_syscall(SYSCALL_FUTEX_WAKEUP, (uintptr_t)val_addr, num_wakeup, IGNORE, IGNORE);
}

long sys_get_timebase()
{
    return invoke_syscall(SYSCALL_GET_TIMEBASE, IGNORE, IGNORE, IGNORE, IGNORE);
}

long sys_get_tick()
{
    return invoke_syscall(SYSCALL_GET_TICK, IGNORE, IGNORE, IGNORE, IGNORE);
}

void sys_process_show(void)
{
    invoke_syscall(SYSCALL_PS, IGNORE, IGNORE, IGNORE, IGNORE);
}

void sys_screen_clear(int line1, int line2)
{
    invoke_syscall(SYSCALL_SCREEN_CLEAR, line1, line2, IGNORE, IGNORE);
}

pid_t sys_getpid()
{
    return invoke_syscall(SYSCALL_GETPID, IGNORE, IGNORE, IGNORE, IGNORE);
}

int sys_get_char()
{
    int ch = -1;
    while (ch == -1) {
        ch = invoke_syscall(SYSCALL_GET_CHAR, IGNORE, IGNORE, IGNORE, IGNORE);
    }
    return ch;
}

void* shmpageget(int key)
{
    return invoke_syscall(SYSCALL_SHMPGET, key, IGNORE, IGNORE, IGNORE);
}

void shmpagedt(void *addr)
{
    invoke_syscall(SYSCALL_SHMPDT, (uintptr_t)addr, IGNORE, IGNORE, IGNORE);
}
