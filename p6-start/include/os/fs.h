#ifndef FS_H
#define FS_H

#include <type.h>
#include <os/list.h>


#define FS_SIZE  0x20000000 // 512M
#define SECTOR_SIZE 512
#define BLOCK_SIZE 4096  //4KB
#define BLOCK_SECTOR_NUM 8

#define BLOCK_NUM 0X20000 // 128k
#define SUPERBLOCK_BLOCK_NUM 1
#define BLOCK_MAP_BLOCK_NUM 4
#define INODE_MAP_BLOCK_NUM 1
#define INODE_BLOCK_NUM 8
#define DATA_BLOCK_NUM (BLOCK_NUM - (SUPERBLOCK_BLOCK_NUM + BLOCK_MAP_BLOCK_NUM + INODE_MAP_BLOCK_NUM + INODE_BLOCK_NUM))

#define BLOCK_MAP_OFFSET (SUPERBLOCK_BLOCK_NUM * BLOCK_SECTOR_NUM)
#define INODE_MAP_OFFSET (BLOCK_MAP_OFFSET + BLOCK_MAP_BLOCK_NUM * BLOCK_SECTOR_NUM)
#define INODE_OFFSET (INODE_MAP_OFFSET + INODE_MAP_BLOCK_NUM * BLOCK_SECTOR_NUM)
#define DATA_OFFSET (INODE_OFFSET + INODE_BLOCK_NUM * BLOCK_SECTOR_NUM)

// location in sd-card    sector count
#define SUPERBLOCK_SECTOR_ADDR  0x100000 // 1M (sector). superblock begin at 512MB of SD card
#define BLOCK_MAP_SECTOR_ADDR   (SUPERBLOCK_SECTOR_ADDR + BLOCK_MAP_OFFSET)
#define INODE_MAP_SECTOR_ADDR   (SUPERBLOCK_SECTOR_ADDR + INODE_MAP_OFFSET)
#define INODE_SECTOR_ADDR       (SUPERBLOCK_SECTOR_ADDR + INODE_OFFSET)
#define DATA_BLOCK_SECTOR_ADDR  (SUPERBLOCK_SECTOR_ADDR + DATA_OFFSET)

#define FS_MAGIC 0x666666666

#define INODE_SIZE 128 // byte
#define INODE_NUM_PER_BLOCK 32
#define INODE_NUM 256

#define FILE_NAME_SIZE 28 // byte
#define DENTRY_SIZE 32 // byte
#define DENTRY_NUM_PER_BLOCK 128

// inode mode
#define FILE_MODE 0
#define DIR_MODE 1
#define SOFT_LINK_MODE 2

// inode data block link
#define L0_BLOCK_COUNT 10
#define L1_BLOCK_COUNT 0x400
#define L2_BLOCK_COUNT 0x100000
#define L3_BLOCK_COUNT 0x40000000

#define L0_BLOCK_UPBOUND L0_BLOCK_COUNT
#define L1_BLOCK_UPBOUND (L0_BLOCK_UPBOUND + L1_BLOCK_COUNT)
#define L2_BLOCK_UPBOUND (L1_BLOCK_UPBOUND + L2_BLOCK_COUNT)
#define L3_BLOCK_UPBOUND (L2_BLOCK_UPBOUND + L3_BLOCK_COUNT)

#define POINT_SIZE 4
#define POINT_PER_BLOCK 1024

// file permission
#define O_RDONLY 1 // read only
#define O_WRONLY 2 // write only 
#define O_RDWR 3 // read&write 

#define BUCKET_NUM 32

typedef struct superblock
{
    uint64_t magic;
    uint64_t size;   
    uint64_t inode_num;
    uint64_t inode_valid;
    uint64_t block_num;
    uint64_t block_valid;
    int inode_map_sector_addr;
    int inode_sector_addr;
    int block_map_sector_addr; 
    int block_sector_addr;
}superblock_t;

typedef struct inode
{
    uint64_t ino;
    uint64_t mode;//directory or file
    uint64_t block_size;   // per block
    uint64_t byte_size;    // valid for file
    uint64_t dentry_num; // valid for dir
    uint64_t time;

    int direct_point0;   // dir/soft link  only use this point
    int direct_point1;
    int direct_point2;
    int direct_point3;
    int direct_point4;
    int direct_point5;
    int direct_point6;
    int direct_point7;
    int direct_point8;
    int direct_point9;

    int first_point; 
    int second_point; 
    int third_point; 
    char place[28]; // valid to make the size of inode_t be 128B
}inode_t;   // 128 B 

typedef struct dentry
{
    char name[FILE_NAME_SIZE];
    int ino;
}dentry_t;  //32 B
typedef struct fd
{
    uint64_t ino;
    int valid;
    int permission; 
    uint64_t read_point; 
    uint64_t write_point; 
}fd_t;

superblock_t temp_superblock;
char m_block_map[4*BLOCK_SIZE];//4 block 
char m_inode_map[BLOCK_SIZE];//1 block

inode_t root;
inode_t cur;

char bufferblock[BLOCK_SIZE];
char temp_block[BLOCK_SIZE];

fd_t fd_open[BUCKET_NUM];

//task1
void do_mkfs();
void do_statfs();
void do_cd(char *name);
void do_mkdir(char *name);
void do_rmdir(char *name);
void do_ls(char *name);
//task2
void do_touch(char *name);
void do_cat(char *name);
int do_fopen(char *name, int permission);
void do_fread(int fd, char *buff, int size);
void do_fwrite(int fd, char *buff, int size);
void do_fclose(int fd);


#endif