#include <os/time.h>
#include <os/mm.h>
#include <os/irq.h>
#include <os/list.h>
#include <os/sched.h>
#include <os/string.h>
#include <os/stdio.h>
#include <type.h>
//#include <pgtable.h>
#include <assert.h>
//#include <pgtable.h>
#include <sbi.h>
#include <os/fs.h>

void memset(void *dest, uint8_t value, uint32_t len)
{
    uint8_t *dst = (uint8_t *)dest;

    for (; len != 0; len--) {
        *dst++ = value;
    }
}

void bzero(void *dest, uint32_t len) { memset(dest, 0, len); }
/*
void refresh_fs(){
        get_inode(&root, 0);//read the root inode into root
       // cur = root;
        sbi_sd_read((unsigned int)m_block_map, 
            BLOCK_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, BLOCK_MAP_SECTOR_ADDR);//read block_map
        sbi_sd_read((unsigned int)m_inode_map, 
            INODE_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, INODE_MAP_SECTOR_ADDR);//read inode_map
};*/
void get_inode(inode_t *dest_inode, int ino)
{
//copy the content of the inode with number ino into dest_inode
    int block = ino / INODE_NUM_PER_BLOCK;//calculate the block id containing the ino 
    int offset = ino % INODE_NUM_PER_BLOCK;//calculate the exact position of ino
    sbi_sd_read((unsigned int)temp_block,BLOCK_SECTOR_NUM, INODE_SECTOR_ADDR+block*BLOCK_SECTOR_NUM);//read the whole block into temp block
    memcpy((uint8_t *)dest_inode, (uint8_t *)(temp_block + offset * INODE_SIZE), INODE_SIZE);//copy the exact inode into dest_inode 
}

void write_inode(inode_t *src_inode, int ino)
{
//
    int block = ino / INODE_NUM_PER_BLOCK;//calculate the block id containing the ino
    int offset = ino % INODE_NUM_PER_BLOCK;//calculate the exact position of ino
    sbi_sd_read((unsigned int)temp_block,BLOCK_SECTOR_NUM, INODE_SECTOR_ADDR+block*BLOCK_SECTOR_NUM);//read the whole block into temp block
    memcpy((uint8_t *)(temp_block + offset * INODE_SIZE), (uint8_t *)src_inode, INODE_SIZE);//modify the exact inode 
    sbi_sd_write((unsigned int)temp_block, BLOCK_SECTOR_NUM, INODE_SECTOR_ADDR+block*BLOCK_SECTOR_NUM); //write back into sd
}

int alloc_block()
{
    int id = 0;
    for (int i = 0; i < 0x4000; i++)
    {
    	//check each bit of the block map 
        if ((m_block_map[i] & 0x1) == 0)
        {
            id = 8 * i + 0;//calculate the number of the first available block
            m_block_map[i] |= 0x1;
            break;
        }
        else if ((m_block_map[i] & 0x2) == 0)
        {
            id = 8 * i + 1;
            m_block_map[i] |= 0x2;
            break;
        }
        else if ((m_block_map[i] & 0x4) == 0)
        {
            id = 8 * i + 2;
            m_block_map[i] |= 0x4;
            break;
        }
        else if ((m_block_map[i] & 0x8) == 0)
        {
            id = 8 * i + 3;
            m_block_map[i] |= 0x8;
            break;
        }
        else if ((m_block_map[i] & 0x10) == 0)
        {
            id = 8 * i + 4;
            m_block_map[i] |= 0x10;
            break;
        }
        else if ((m_block_map[i] & 0x20) == 0)
        {
            id = 8 * i + 5;
            m_block_map[i] |= 0x20;
            break;
        }
        else if ((m_block_map[i] & 0x40) == 0)
        {
            id = 8 * i + 6;
            m_block_map[i] |= 0x40;
            break;
        }
        else if ((m_block_map[i] & 0x80) == 0)
        {
            id = 8 * i + 7;
            m_block_map[i] |= 0x80;
            break;
        }
    }
 
    temp_superblock.block_valid++;
    int block_id = DATA_BLOCK_SECTOR_ADDR + id*BLOCK_SECTOR_NUM;
    return block_id;//return the number of the beginning sector
}
int alloc_inode()//similar to  alloc block
{
    int id = 0;
    for (int i = 0; i < 0x1000; i++)
    {
        if ((m_inode_map[i] & 0x1) == 0)
        {
            id = 8 * i + 0;
            m_inode_map[i] |= 0x1;
            break;
        }
        else if ((m_inode_map[i] & 0x2) == 0)
        {
            id = 8 * i + 1;
            m_inode_map[i] |= 0x2;
            break;
        }
        else if ((m_inode_map[i] & 0x4) == 0)
        {
            id = 8 * i + 2;
            m_inode_map[i] |= 0x4;
            break;
        }
        else if ((m_inode_map[i] & 0x8) == 0)
        {
            id = 8 * i + 3;
            m_inode_map[i] |= 0x8;
            break;
        }
        else if ((m_inode_map[i] & 0x10) == 0)
        {
            id = 8 * i + 4;
            m_inode_map[i] |= 0x10;
            break;
        }
        else if ((m_inode_map[i] & 0x20) == 0)
        {
            id = 8 * i + 5;
            m_inode_map[i] |= 0x20;
            break;
        }
        else if ((m_inode_map[i] & 0x40) == 0)
        {
            id = 8 * i + 6;
            m_inode_map[i] |= 0x40;
            break;
        }
        else if ((m_inode_map[i] & 0x80) == 0)
        {
            id = 8 * i + 7;
            m_inode_map[i] |= 0x80;
            break;
        }
    }
    temp_superblock.inode_valid++;
    return id;//return the ino
}
void free_block(int block_addr)
{
    int id = (block_addr - DATA_BLOCK_SECTOR_ADDR)/BLOCK_SECTOR_NUM;//number of the block
    m_block_map[id/8] &= ~(1<<(id%8));//modify the bit in block map 
    temp_superblock.block_valid--;
}



void free_inode(int index)
{
    m_inode_map[index/8] &= ~(1<<(index%8));
    temp_superblock.inode_valid--;
}

// initialize file system
void initial_fs()
{
    prints(" Start initializing our filesystem!\r");
    // initialize superblock
    temp_superblock.magic = FS_MAGIC;
    temp_superblock.size = FS_SIZE;
    temp_superblock.inode_num = INODE_NUM;
    temp_superblock.inode_valid = 0;
    temp_superblock.block_num = BLOCK_NUM;
    temp_superblock.block_valid = SUPERBLOCK_BLOCK_NUM + BLOCK_MAP_BLOCK_NUM + INODE_MAP_BLOCK_NUM + INODE_BLOCK_NUM;
    temp_superblock.block_map_sector_addr = BLOCK_MAP_SECTOR_ADDR;
    temp_superblock.inode_map_sector_addr = INODE_MAP_SECTOR_ADDR;
    temp_superblock.inode_sector_addr = INODE_SECTOR_ADDR;
    temp_superblock.block_sector_addr = DATA_BLOCK_SECTOR_ADDR;

    prints("     magic number: 0x%lx\r", temp_superblock.magic);
    prints("     total block : %d, start sector : %d\r", BLOCK_NUM, SUPERBLOCK_SECTOR_ADDR);
    prints("     block map addr offset : %d (%d)\r", BLOCK_MAP_OFFSET, BLOCK_MAP_BLOCK_NUM);
    prints("     data addr offset : %d (%d)\r", DATA_OFFSET, DATA_BLOCK_NUM);
    prints("     inode size : %d Bytes\r", INODE_SIZE);
    prints("     inode map addr offset : %d (%d)\r", INODE_MAP_OFFSET, INODE_MAP_BLOCK_NUM);
    prints("     inode addr offset : %d (%d)\r", INODE_OFFSET, INODE_BLOCK_NUM);
    prints("     dentry size : %d Bytes\r", DENTRY_SIZE);

    // block map
    bzero(m_block_map, 0x4000);
    int root_block_num = alloc_block();
    sbi_sd_write((unsigned int)m_block_map, 
        BLOCK_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, BLOCK_MAP_SECTOR_ADDR);//initialize block map in sd
    
    // inode map
    prints("[FS] Setting inode-map...\r");
    bzero(m_inode_map, 0x1000);
    int root_ino = alloc_inode();   // root ino must be 0,����Ŀ¼����inode 
    sbi_sd_write((unsigned int)m_inode_map, 
        INODE_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, INODE_MAP_SECTOR_ADDR);

    // set root inode
    root.mode = DIR_MODE;
    root.ino = root_ino;
    root.block_size = 1;
    root.dentry_num = 2; // .. and .
    root.direct_point0 = root_block_num;
    root.time = get_timer();

    cur = root;//��ʼ����Ŀ¼��inode 

    int block_id = root_ino / INODE_NUM_PER_BLOCK;
    int offset = root_ino % INODE_NUM_PER_BLOCK; 

    for (int i = 0; i < INODE_BLOCK_NUM; i++)//write zero to inode district in sd 
    {
        bzero(bufferblock, BLOCK_SIZE);
        if (i == block_id)
            memcpy((uint8_t *)(bufferblock + offset * INODE_SIZE), 
                (uint8_t *)&root, INODE_SIZE);
        sbi_sd_write((unsigned int)bufferblock, 
            BLOCK_SECTOR_NUM, INODE_SECTOR_ADDR + i*BLOCK_SECTOR_NUM);
    }

    // data block
    bzero(bufferblock, BLOCK_SIZE);
    dentry_t temp;
    //set two dentry:.&..
    bzero(temp.name, FILE_NAME_SIZE);
    char name1[10] = "..";
    memcpy((uint8_t *)temp.name, (uint8_t *)name1, 2);
    temp.ino = root_ino;
    memcpy((uint8_t *)bufferblock, (uint8_t *)&temp, DENTRY_SIZE);

    bzero(temp.name, FILE_NAME_SIZE);
    char name2[10] = ".";
    memcpy((uint8_t *)temp.name, (uint8_t *)name2, 1);
    temp.ino = root_ino;
    memcpy((uint8_t *)(bufferblock + DENTRY_SIZE), (uint8_t *)&temp, DENTRY_SIZE);
    //write . and .. into sd
    sbi_sd_write((unsigned int)bufferblock, 
        BLOCK_SECTOR_NUM, root_block_num);

    // write superblock into sd
    bzero(bufferblock, BLOCK_SIZE);
    memcpy((uint8_t *)bufferblock, (uint8_t *)&temp_superblock, sizeof(superblock_t));
    sbi_sd_write((unsigned int)bufferblock, 
        SUPERBLOCK_BLOCK_NUM*BLOCK_SECTOR_NUM, SUPERBLOCK_SECTOR_ADDR);

    prints(" Initializing completed!\r");
}


void do_mkfs()
{
    bzero(bufferblock, BLOCK_SIZE);
    sbi_sd_read((unsigned int)bufferblock, SUPERBLOCK_BLOCK_NUM*BLOCK_SECTOR_NUM, SUPERBLOCK_SECTOR_ADDR);//read superblock
    memcpy((uint8_t *)&temp_superblock, (uint8_t *)bufferblock, sizeof(superblock_t));//read superblock into temp_superblock
    if (temp_superblock.magic != FS_MAGIC)//check if fs already exists
    {
        initial_fs();
    }
    else
    {
        prints("File System already exits! \r");
        get_inode(&root, 0);//read the root inode into root
        cur = root;
        sbi_sd_read((unsigned int)m_block_map, 
            BLOCK_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, BLOCK_MAP_SECTOR_ADDR);//read block_map
        sbi_sd_read((unsigned int)m_inode_map, 
            INODE_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, INODE_MAP_SECTOR_ADDR);//read inode_map
    }
    for (int i = 0; i < BUCKET_NUM; i++)
    {
        fd_open[i].valid = 0;
    }    
}

void do_statfs()//statfs 
{
    prints("[FS] INFO:\r");
    prints("     magic number: 0x%lx\r", temp_superblock.magic);
    prints("     valid block : %d/%d, start sector : %d\r", temp_superblock.block_valid, 
            BLOCK_NUM, SUPERBLOCK_SECTOR_ADDR);
    prints("     block map offset : %d (%d)\r", BLOCK_MAP_OFFSET, BLOCK_MAP_BLOCK_NUM);
    prints("     inode map offset : %d (%d), valid : %d/%d\r", INODE_MAP_OFFSET, 
            INODE_MAP_BLOCK_NUM, temp_superblock.inode_valid, INODE_NUM);
    prints("     inode offset : %d (%d)\r", INODE_OFFSET, INODE_BLOCK_NUM);
    prints("     data offset : %d (%d)\r", DATA_OFFSET, DATA_BLOCK_NUM);
    prints("     inode entry size : %dB\r", INODE_SIZE);
    prints("     dir entry size : %dB\r", DENTRY_SIZE);

}

void do_cd(char *name)//cd
{
    int len = strlen(name);
    if (len == 0)
        return;
    
    int dir_count = 1;
    char *dir[10] = {0};
    int root_begin = 0;
    if (name[0] == '/')
        root_begin = 1;  //type of the path 
    if (name[0] == '/'&&name[1] == '\0'){
      cur = root;
      return ;
    }
    
    for (int i = 1; i < len; i++)
    {
        if (name[i] == '/')
            dir_count++;
    }//count the depth of the path
    char buf[64];
    bzero(buf, 64);
    buf[0] = '/';
    if (name[0] == '/')
        strcpy(buf, name);
    else
        strcpy(&buf[1], name);
    
    int index = 0;
    for (int i = 0; i < len+1; i++)
    {
        if (buf[i] == '/')
            buf[i] = 0;
        else if (buf[i-1] == 0)
            dir[index++] = &buf[i];
    }
    
    // begin search
    inode_t temp_inode;
    int block_id;
    dentry_t temp_dentry;

    if (root_begin)
        temp_inode = root;
    else
        temp_inode = cur;

    for (int j = 0; j < dir_count; j++)//parse the path
    {
        int found = 0;
        block_id = temp_inode.direct_point0;
        bzero(bufferblock, BLOCK_SIZE);
        sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);//read the block

        for (int i = 0; i < temp_inode.dentry_num; i++)//look every dentry to find match 
        {
            memcpy((uint8_t *)&temp_dentry, (uint8_t *)(bufferblock + i * DENTRY_SIZE), DENTRY_SIZE);//��Ŀ¼�����temp_dentry 
            if (strcmp(temp_dentry.name, dir[j]) == 0)//see if the name match
            {
                found = 1;
                break;
            }
        }
        if (!found)
        {
            prints("ERROR! not found!\r");
            return;
        }
        get_inode(&temp_inode, temp_dentry.ino);//get the inode of the found dir 
        if (temp_inode.mode != DIR_MODE && temp_inode.mode != SOFT_LINK_MODE)
        {
            prints("ERROR! it is not a dir!\r");        
            return;
        }

        // soft link
        if (temp_inode.mode == SOFT_LINK_MODE)
        {
            bzero(bufferblock, BLOCK_SIZE);
            sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, temp_inode.direct_point0);
            do_cd(bufferblock);//recurrence
            return;
        }
    }

    cur = temp_inode;
}

void do_mkdir(char *name)
{
    int block_id = cur.direct_point0;
    bzero(bufferblock, BLOCK_SIZE);
    sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);

    int found = 0;
    dentry_t temp_dentry;
    for (int i = 0; i < cur.dentry_num; i++)
    {
        memcpy((uint8_t *)&temp_dentry, (uint8_t *)(bufferblock + i * DENTRY_SIZE), DENTRY_SIZE);
        if (strcmp(temp_dentry.name, name) == 0)
        {
            found = 1;
            break;
        }
    }//see if the dir already exists
    if (!found)
    {
        int mk_block_loc = alloc_block();

        // new dir inode
        int mk_ino = alloc_inode();
        inode_t mk_inode;
        mk_inode.mode = DIR_MODE;
        mk_inode.ino = mk_ino;
        mk_inode.block_size = 1;
        mk_inode.dentry_num = 2;
        mk_inode.direct_point0 = mk_block_loc;
        mk_inode.time = get_timer();
        write_inode(&mk_inode, mk_ino);//write new inode into sd 

        // new dir data block
        bzero(temp_block, BLOCK_SIZE);

        bzero(temp_dentry.name, FILE_NAME_SIZE);

        //build dentry ..&.
        char name1[5] = "..";
        memcpy((uint8_t *)temp_dentry.name, (uint8_t *)name1, 2);
        temp_dentry.ino = cur.ino;
        memcpy((uint8_t *)temp_block, (uint8_t *)&temp_dentry, DENTRY_SIZE); 

        bzero(temp_dentry.name, FILE_NAME_SIZE);
        char name2[5] = ".";
        memcpy((uint8_t *)temp_dentry.name, (uint8_t *)name2, 1);
        temp_dentry.ino = mk_ino;
        memcpy((uint8_t *)(temp_block + DENTRY_SIZE), (uint8_t *)&temp_dentry, DENTRY_SIZE);
        sbi_sd_write((unsigned int)temp_block, BLOCK_SECTOR_NUM, mk_block_loc);//write dentry into sd
        // build dentry name 
        bzero(temp_dentry.name, FILE_NAME_SIZE);
        strcpy(temp_dentry.name, name);
        temp_dentry.ino = mk_ino;
        memcpy((uint8_t *)(bufferblock + cur.dentry_num * DENTRY_SIZE), 
            (uint8_t *)&temp_dentry, DENTRY_SIZE);
        sbi_sd_write((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);//write new dentry into sd

        cur.dentry_num++;
        cur.time = get_timer();
        if (cur.ino == root.ino)
        {
            root.dentry_num = cur.dentry_num;
            root.time = cur.time;
        }
        write_inode(&cur, cur.ino);//refresh cur

        // refresh superblock &map
        bzero(bufferblock, BLOCK_SIZE);
        memcpy((uint8_t *)bufferblock, (uint8_t *)&temp_superblock, sizeof(superblock_t));
        sbi_sd_write((unsigned int)bufferblock, SUPERBLOCK_BLOCK_NUM*BLOCK_SECTOR_NUM, SUPERBLOCK_SECTOR_ADDR);
        sbi_sd_write((unsigned int)m_inode_map, INODE_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, INODE_MAP_SECTOR_ADDR);
        sbi_sd_write((unsigned int)m_block_map, BLOCK_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, BLOCK_MAP_SECTOR_ADDR);
        prints("create success!!!\r");
        get_inode(&root, 0);//read the root inode into root
       // cur = root;
        get_inode(&cur, cur.ino);
        sbi_sd_read((unsigned int)m_block_map,BLOCK_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, BLOCK_MAP_SECTOR_ADDR);//read block_map
        sbi_sd_read((unsigned int)m_inode_map,INODE_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, INODE_MAP_SECTOR_ADDR);//read inode_map
    }
    else
        prints("ERROR!!! dir already exists!!!\r");
}

void do_rmdir(char *name)
{
    // similar to mkdir
    int block_id = cur.direct_point0;
    bzero(bufferblock, BLOCK_SIZE);
    sbi_sd_read((unsigned int)bufferblock, 
        BLOCK_SECTOR_NUM, block_id);

    int found = 0;
    dentry_t dentry;
    for (int i = 0; i < cur.dentry_num; i++)
    {
        memcpy((uint8_t *)&dentry, (uint8_t *)(bufferblock + i * DENTRY_SIZE), DENTRY_SIZE);
        if (!strcmp(dentry.name, name))//match name
        {
            found = 1;
            inode_t temp_inode;
            get_inode(&temp_inode, dentry.ino);
            if (temp_inode.mode != DIR_MODE)
            {
                prints("ERROR!!! not a dir!!!\r");    
                return;    
            }
            free_block(temp_inode.direct_point0);
            free_inode(dentry.ino);

            // write back parent block & inode
            bzero(temp_block, BLOCK_SIZE);
            memcpy((uint8_t *)temp_block, (uint8_t *)bufferblock, DENTRY_SIZE * i);
            memcpy((uint8_t *)(temp_block+DENTRY_SIZE*i), (uint8_t *)(bufferblock + DENTRY_SIZE*(i+1)), DENTRY_SIZE * (cur.dentry_num-(i+1)));
            sbi_sd_write((unsigned int)temp_block, BLOCK_SECTOR_NUM, block_id);

            cur.dentry_num--;
            cur.time = get_timer();
            if (cur.ino == root.ino)
            {
                root.dentry_num = cur.dentry_num;
                root.time = cur.time;
            }//refresh cur ino
            write_inode(&cur, cur.ino);//write back cur ino

            // change superblock & block map & inode map and write them back to sd
            bzero(bufferblock, BLOCK_SIZE);
            memcpy((uint8_t *)bufferblock, (uint8_t *)&temp_superblock, sizeof(superblock_t));
            sbi_sd_write((unsigned int)bufferblock, SUPERBLOCK_BLOCK_NUM*BLOCK_SECTOR_NUM, SUPERBLOCK_SECTOR_ADDR);
            sbi_sd_write((unsigned int)m_block_map, BLOCK_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, BLOCK_MAP_SECTOR_ADDR);
            sbi_sd_write((unsigned int)m_inode_map, INODE_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, INODE_MAP_SECTOR_ADDR);

            break;
        }
    }
    if (!found)
        prints("ERROR!!! dir not found!!!\r");
    else
        prints("remove dir success!!!\r");
}

void do_ls(char *name)//ls 
{
    dentry_t dentry;
    int block_id = cur.direct_point0;
    bzero(bufferblock, BLOCK_SIZE);
    sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);
    for (int i = 0; i < cur.dentry_num; i++)//see all the dentry and prints
    {
      memcpy((uint8_t *)&dentry, (uint8_t *)(bufferblock + i * DENTRY_SIZE), DENTRY_SIZE);
      prints("%s  ", dentry.name);
    }
    prints("\r");
    return;
}


void print_content(inode_t *src_inode, int block_num, int print_size)
{
    int block_id;
    switch (block_num)
    {
    // l0
    case 0: block_id = src_inode->direct_point0; break;
    case 1: block_id = src_inode->direct_point1; break;
    case 2: block_id = src_inode->direct_point2; break;
    case 3: block_id = src_inode->direct_point3; break;
    case 4: block_id = src_inode->direct_point4; break;
    case 5: block_id = src_inode->direct_point5; break;
    case 6: block_id = src_inode->direct_point6; break;
    case 7: block_id = src_inode->direct_point7; break;
    case 8: block_id = src_inode->direct_point8; break;
    case 9: block_id = src_inode->direct_point9; break;
    default:
        if (block_num < 0)
        {
            prints("ERROR!!! print out of file border!!!");
            return;
        }
        // l1
        else if (block_num < L1_BLOCK_UPBOUND)//first indirect
        {
            block_num -= L0_BLOCK_UPBOUND;
            bzero(bufferblock, BLOCK_SIZE);
            sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, src_inode->first_point);
            memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+block_num*POINT_SIZE), POINT_SIZE);
        }
        // l2
        else if (block_num < L2_BLOCK_UPBOUND)//second indirect
        {
            block_num -= L1_BLOCK_UPBOUND;
            int l1_block_num = block_num / L1_BLOCK_COUNT;
            int l0_block_num = block_num % L1_BLOCK_COUNT;

            bzero(bufferblock, BLOCK_SIZE);
            sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, src_inode->second_point);
            memcpy((uint8_t *)&block_id,(uint8_t *)(bufferblock+l1_block_num*POINT_SIZE), POINT_SIZE);

            sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, block_id);
            memcpy((uint8_t *)&block_id,(uint8_t *)(bufferblock+l0_block_num*POINT_SIZE), POINT_SIZE);//read the content block into buffer
        }
        // l3
        else if (block_num < L3_BLOCK_UPBOUND)//third indirect
        {
            block_num -= L2_BLOCK_UPBOUND;
            int l2_block_num = block_num / L2_BLOCK_COUNT;
            int l1_block_num = (block_num % L2_BLOCK_COUNT) / L1_BLOCK_COUNT;
            int l0_block_num = (block_num % L2_BLOCK_COUNT) % L1_BLOCK_COUNT;

            bzero(bufferblock, BLOCK_SIZE);
            sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, src_inode->third_point);
            memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+l2_block_num*POINT_SIZE), POINT_SIZE);

            sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, block_id);
            memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+l1_block_num*POINT_SIZE), POINT_SIZE);

            sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, block_id);
            memcpy((uint8_t *)&block_id,  (uint8_t *)(bufferblock+l0_block_num*POINT_SIZE), POINT_SIZE);
        }
        else
        {
            prints("ERROR!!! print out of file border!!!");
            return;
        }
        break;
    }
    bzero(bufferblock, BLOCK_SIZE);
    sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);
    for (int i = 0; i < print_size; i++)
        prints("%c", bufferblock[i]);
}

void read_content(inode_t *src_inode, int block_num, int begin_point, int read_size, char *buff)
{
//similar as print_content
    int block_id;
    switch (block_num)
    {
    // l0
    case 0: block_id = src_inode->direct_point0; break;
    case 1: block_id = src_inode->direct_point1; break;
    case 2: block_id = src_inode->direct_point2; break;
    case 3: block_id = src_inode->direct_point3; break;
    case 4: block_id = src_inode->direct_point4; break;
    case 5: block_id = src_inode->direct_point5; break;
    case 6: block_id = src_inode->direct_point6; break;
    case 7: block_id = src_inode->direct_point7; break;
    case 8: block_id = src_inode->direct_point8; break;
    case 9: block_id = src_inode->direct_point9; break;
    default:
        if (block_num < 0)
        {
            prints("ERROR!!! read out of file border!!!");
            return;
        }
        // l1
        else if (block_num < L1_BLOCK_UPBOUND)
        {
            block_num -= L0_BLOCK_UPBOUND;

            bzero(bufferblock, BLOCK_SIZE);
            sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, src_inode->first_point);
            memcpy((uint8_t *)&block_id,(uint8_t *)(bufferblock+block_num*POINT_SIZE), POINT_SIZE);
        }
        // l2
        else if (block_num < L2_BLOCK_UPBOUND)
        {
            block_num -= L1_BLOCK_UPBOUND;
            int l1_block_num = block_num / L1_BLOCK_COUNT;
            int l0_block_num = block_num % L1_BLOCK_COUNT;

            bzero(bufferblock, BLOCK_SIZE);
            sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, src_inode->second_point);
            memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+l1_block_num*POINT_SIZE), POINT_SIZE);

            sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);
            memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+l0_block_num*POINT_SIZE), POINT_SIZE);
        }
        // l3
        else if (block_num < L3_BLOCK_UPBOUND)
        {
            block_num -= L2_BLOCK_UPBOUND;
            int l2_block_num = block_num / L2_BLOCK_COUNT;
            int l1_block_num = (block_num % L2_BLOCK_COUNT) / L1_BLOCK_COUNT;
            int l0_block_num = (block_num % L2_BLOCK_COUNT) % L1_BLOCK_COUNT;

            bzero(bufferblock, BLOCK_SIZE);
            sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, src_inode->third_point);
            memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+l2_block_num*POINT_SIZE), POINT_SIZE);

            sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);
            memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+l1_block_num*POINT_SIZE), POINT_SIZE);

            sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);
            memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+l0_block_num*POINT_SIZE), POINT_SIZE);
        }
        else
        {
            prints("ERROR!!! read out of file border!!!");
            return;
        }
        break;
    }

    bzero(bufferblock, BLOCK_SIZE);
    sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);

    memcpy((uint8_t *)buff, (uint8_t *)(bufferblock+begin_point), read_size);
}

void write_content(inode_t *dest_inode, int block_num, int begin_point, int write_size, char *buff)
{
    int block_id;

    if (block_num >= dest_inode->block_size)
    {
        int new_block_loc = alloc_block();
        bzero(bufferblock, BLOCK_SIZE);
        memcpy((uint8_t *)(bufferblock+begin_point), (uint8_t *)buff, write_size);
        sbi_sd_write((unsigned int)bufferblock, BLOCK_SECTOR_NUM, new_block_loc);

        switch (block_num)
        {
        // l0
        case 0: dest_inode->direct_point0 = new_block_loc; break;
        case 1: dest_inode->direct_point1 = new_block_loc; break;
        case 2: dest_inode->direct_point2 = new_block_loc; break;
        case 3: dest_inode->direct_point3 = new_block_loc; break;
        case 4: dest_inode->direct_point4 = new_block_loc; break;
        case 5: dest_inode->direct_point5 = new_block_loc; break;
        case 6: dest_inode->direct_point6 = new_block_loc; break;
        case 7: dest_inode->direct_point7 = new_block_loc; break;
        case 8: dest_inode->direct_point8 = new_block_loc; break;
        case 9: dest_inode->direct_point9 = new_block_loc; break;
        default:
            if (block_num < 0)
            {
                prints("ERROR!!! write out of file border!!!");
                return;
            }
            // l1
            else if (block_num < L1_BLOCK_UPBOUND)
            {
                // allocate l1 indirect point block
                if (block_num == L0_BLOCK_UPBOUND)
                {
                    int new_l1_block = alloc_block();
                    dest_inode->first_point = new_l1_block;
                    bzero(bufferblock, BLOCK_SIZE);
                    sbi_sd_write((unsigned int)bufferblock, BLOCK_SECTOR_NUM, dest_inode->first_point);
                }
                
                block_num -= L0_BLOCK_UPBOUND;

                bzero(bufferblock, BLOCK_SIZE);
                sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, dest_inode->first_point);
                memcpy((uint8_t *)(bufferblock+block_num*POINT_SIZE),(uint8_t *)&new_block_loc, POINT_SIZE);
                sbi_sd_write((unsigned int)bufferblock, BLOCK_SECTOR_NUM, dest_inode->first_point);
            }
            // l2
            else if (block_num < L2_BLOCK_UPBOUND)
            {
                // allocate l2 indirect point block
                if (block_num == L1_BLOCK_UPBOUND)
                {
                    int new_l2_block = alloc_block();
                    dest_inode->second_point = new_l2_block;
                    bzero(bufferblock, BLOCK_SIZE);
                    sbi_sd_write((unsigned int)bufferblock,BLOCK_SECTOR_NUM, dest_inode->second_point);
                }
                
                block_num -= L1_BLOCK_UPBOUND;
                int l1_block_num = block_num / L1_BLOCK_COUNT;
                int l0_block_num = block_num % L1_BLOCK_COUNT;

                // allocate l1 indirect point block
                if (l0_block_num == 0)
                {
                    int new_l1_block = alloc_block();

                    bzero(bufferblock, BLOCK_SIZE);
                    sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, dest_inode->second_point);

                    memcpy((uint8_t *)(bufferblock+l1_block_num*POINT_SIZE),(uint8_t *)&new_l1_block, POINT_SIZE);
                    sbi_sd_write((unsigned int)bufferblock,BLOCK_SECTOR_NUM, dest_inode->second_point);
                }
                
                bzero(bufferblock, BLOCK_SIZE);
                sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, dest_inode->second_point);
                memcpy((uint8_t *)&block_id,(uint8_t *)(bufferblock+l1_block_num*POINT_SIZE), POINT_SIZE);

                sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);
                memcpy((uint8_t *)(bufferblock+l0_block_num*POINT_SIZE),(uint8_t *)&new_block_loc, POINT_SIZE);
                sbi_sd_write((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);
            }
            // l3
            else if (block_num < L3_BLOCK_UPBOUND)
            {
                // allocate l3 indirect point block
                if (block_num == L2_BLOCK_UPBOUND)
                {
                    int new_l3_block = alloc_block();
                    dest_inode->third_point = new_l3_block;
                    bzero(bufferblock, BLOCK_SIZE);
                    sbi_sd_write((unsigned int)bufferblock,BLOCK_SECTOR_NUM, dest_inode->third_point);
                }

                block_num -= L2_BLOCK_UPBOUND;
                int l2_block_num = block_num / L2_BLOCK_COUNT;
                int l1_block_num = (block_num % L2_BLOCK_COUNT) / L1_BLOCK_COUNT;
                int l0_block_num = (block_num % L2_BLOCK_COUNT) % L1_BLOCK_COUNT;

                // allocate l2 indirect point block
                if (l1_block_num == 0 && l0_block_num == 0)
                {
                    int new_l2_block = alloc_block();

                    bzero(bufferblock, BLOCK_SIZE);
                    sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, dest_inode->third_point);

                    memcpy((uint8_t *)(bufferblock+l2_block_num*POINT_SIZE), (uint8_t *)&new_l2_block, POINT_SIZE);
                    sbi_sd_write((unsigned int)bufferblock,BLOCK_SECTOR_NUM, dest_inode->third_point);
                }
                // allocate l1 indirect point block
                if (l0_block_num == 0)
                {
                    int new_l1_block = alloc_block();

                    bzero(bufferblock, BLOCK_SIZE);
                    sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, dest_inode->third_point);

                    memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+l2_block_num*POINT_SIZE), POINT_SIZE);
                    sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);
                        
                    memcpy((uint8_t *)(bufferblock+l1_block_num*POINT_SIZE),(uint8_t *)&new_l1_block, POINT_SIZE);
                    sbi_sd_write((unsigned int)bufferblock,BLOCK_SECTOR_NUM, block_id);
                }

                bzero(bufferblock, BLOCK_SIZE);
                sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, dest_inode->third_point);
                memcpy((uint8_t *)&block_id,(uint8_t *)(bufferblock+l2_block_num*POINT_SIZE), POINT_SIZE);

                sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, block_id);
                memcpy((uint8_t *)&block_id,(uint8_t *)(bufferblock+l1_block_num*POINT_SIZE), POINT_SIZE);

                sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, block_id);
                memcpy((uint8_t *)(bufferblock+l0_block_num*POINT_SIZE),(uint8_t *)&new_block_loc, POINT_SIZE);
                sbi_sd_write((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);
            }
            else
            {
                prints("ERROR!!! write out of file border!!!");
                return;
            }
            break;
        }
        dest_inode->block_size++;
        dest_inode->byte_size += write_size;
        dest_inode->time = get_timer();

        write_inode(dest_inode, dest_inode->ino);

        bzero(bufferblock, BLOCK_SIZE);
        memcpy((uint8_t *)bufferblock, (uint8_t *)&temp_superblock, sizeof(superblock_t));
        sbi_sd_write((unsigned int)bufferblock,SUPERBLOCK_BLOCK_NUM*BLOCK_SECTOR_NUM, SUPERBLOCK_SECTOR_ADDR);
        sbi_sd_write((unsigned int)m_block_map, BLOCK_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, BLOCK_MAP_SECTOR_ADDR);
    }
    else
    {
        switch (block_num)
        {
        // l0
        case 0: block_id = dest_inode->direct_point0; break;
        case 1: block_id = dest_inode->direct_point1; break;
        case 2: block_id = dest_inode->direct_point2; break;
        case 3: block_id = dest_inode->direct_point3; break;
        case 4: block_id = dest_inode->direct_point4; break;
        case 5: block_id = dest_inode->direct_point5; break;
        case 6: block_id = dest_inode->direct_point6; break;
        case 7: block_id = dest_inode->direct_point7; break;
        case 8: block_id = dest_inode->direct_point8; break;
        case 9: block_id = dest_inode->direct_point9; break;
        default:
            if (block_num < 0)
            {
                prints("ERROR!!! write out of file boundary!!!");
                return;
            }
            // l1
            else if (block_num < L1_BLOCK_UPBOUND)
            {
                block_num -= L0_BLOCK_UPBOUND;
                bzero(bufferblock, BLOCK_SIZE);
                sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, dest_inode->first_point);
                memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+block_num*POINT_SIZE), POINT_SIZE);
            }
            // l2
            else if (block_num < L2_BLOCK_UPBOUND)
            {
                block_num -= L1_BLOCK_UPBOUND;
                int l1_block_num = block_num / L1_BLOCK_COUNT;
                int l0_block_num = block_num % L1_BLOCK_COUNT;

                bzero(bufferblock, BLOCK_SIZE);
                sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, dest_inode->second_point);
                memcpy((uint8_t *)&block_id,(uint8_t *)(bufferblock+l1_block_num*POINT_SIZE), POINT_SIZE);

                sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);
                memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+l0_block_num*POINT_SIZE), POINT_SIZE);
            }
            // l3
            else if (block_num < L3_BLOCK_UPBOUND)
            {
                block_num -= L2_BLOCK_UPBOUND;
                int l2_block_num = block_num / L2_BLOCK_COUNT;
                int l1_block_num = (block_num % L2_BLOCK_COUNT) / L1_BLOCK_COUNT;
                int l0_block_num = (block_num % L2_BLOCK_COUNT) % L1_BLOCK_COUNT;

                bzero(bufferblock, BLOCK_SIZE);
                sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, dest_inode->third_point);
                memcpy((uint8_t *)&block_id,(uint8_t *)(bufferblock+l2_block_num*POINT_SIZE), POINT_SIZE);

                sbi_sd_read((unsigned int)bufferblock,  BLOCK_SECTOR_NUM, block_id);
                memcpy((uint8_t *)&block_id,(uint8_t *)(bufferblock+l1_block_num*POINT_SIZE), POINT_SIZE);

                sbi_sd_read((unsigned int)bufferblock, BLOCK_SECTOR_NUM, block_id);
                memcpy((uint8_t *)&block_id, (uint8_t *)(bufferblock+l0_block_num*POINT_SIZE), POINT_SIZE);
            }
            else
            {
                prints("ERROR!!! write out of file border!!!");
                return;
            }
            break;
        }

        bzero(bufferblock, BLOCK_SIZE);
        sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, block_id);
        memcpy((uint8_t *)(bufferblock+begin_point), (uint8_t *)buff, write_size);
        sbi_sd_write((unsigned int)bufferblock,BLOCK_SECTOR_NUM, block_id);

        dest_inode->byte_size += write_size;
        dest_inode->time = get_timer();
        write_inode(dest_inode, dest_inode->ino);

    }
}

void do_touch(char *name)
{
    int block_id = cur.direct_point0;
    bzero(bufferblock, BLOCK_SIZE);
    sbi_sd_read((unsigned int)bufferblock, 
        BLOCK_SECTOR_NUM, block_id);

    int found = 0;
    dentry_t temp_dentry;
    for (int i = 0; i < cur.dentry_num; i++)
    {
        memcpy((uint8_t *)&temp_dentry, (uint8_t *)(bufferblock + i * DENTRY_SIZE), DENTRY_SIZE);
        if (strcmp(temp_dentry.name, name) == 0)
        {
            found = 1;
            break;
        }
    }
    if (!found)
    {
        // new file inode
        int new_ino = alloc_inode();
        inode_t new_inode;
        new_inode.mode = FILE_MODE;
        new_inode.ino = new_ino;
        new_inode.block_size = 0;
        new_inode.byte_size = 0;
        new_inode.time = get_timer();
        write_inode(&new_inode, new_ino);

        // change parent dir data block
        bzero(temp_dentry.name, FILE_NAME_SIZE);
        strcpy(temp_dentry.name, name);
        temp_dentry.ino = new_ino;
        memcpy((uint8_t *)(bufferblock + cur.dentry_num * DENTRY_SIZE), 
            (uint8_t *)&temp_dentry, DENTRY_SIZE);
        sbi_sd_write((unsigned int)bufferblock, 
            BLOCK_SECTOR_NUM, block_id);

        // change parent inode
        cur.dentry_num++;
        cur.time = get_timer();
        if (cur.ino == root.ino)
        {
            root.dentry_num = cur.dentry_num;
            root.time = cur.time;
        }
        write_inode(&cur, cur.ino);

        // change superblock & block map & inode map
        bzero(bufferblock, BLOCK_SIZE);
        memcpy((uint8_t *)bufferblock, (uint8_t *)&temp_superblock, sizeof(superblock_t));
        sbi_sd_write((unsigned int)bufferblock, 
            SUPERBLOCK_BLOCK_NUM*BLOCK_SECTOR_NUM, SUPERBLOCK_SECTOR_ADDR);

        sbi_sd_write((unsigned int)m_block_map, 
            BLOCK_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, BLOCK_MAP_SECTOR_ADDR);
        sbi_sd_write((unsigned int)m_inode_map, 
            INODE_MAP_BLOCK_NUM*BLOCK_SECTOR_NUM, INODE_MAP_SECTOR_ADDR);
        prints("create file success!!!\r");
    }
    else
        prints("ERROR!!! file already exists!!!\r");
}

void do_cat(char *name)
{
    // find dir where the file lies
    int length = strlen(name);
    if (length == 0)
        return;
    
    int dir_count = 1;
    char *dirv[10] = {0};
    int root_begin = 0;
    if (name[0] == '/')
        root_begin = 1;    
    
    for (int i = 1; i < length; i++)
    {
        if (name[i] == '/')
            dir_count++;
    }
    char temp_buf[64];
    bzero(temp_buf, 64);
    temp_buf[0] = '/';
    if (name[0] == '/')
        strcpy(temp_buf, name);
    else
        strcpy(&temp_buf[1], name);
    
    int temp_dirc = 0;
    for (int i = 0; i < length+1; i++)
    {
        if (temp_buf[i] == '/')
            temp_buf[i] = 0;
        else if (temp_buf[i-1] == 0)
            dirv[temp_dirc++] = &temp_buf[i];
    }
    
    name = dirv[dir_count-1];

    // begin search
    inode_t temp_inode;
    int block_id;
    int found;
    dentry_t temp_dentry;

    if (root_begin)
        temp_inode = root;
    else
        temp_inode = cur;

    for (int j = 0; j < dir_count-1; j++) // attention dir_count-1 means the last path name is a file
    {
        block_id = temp_inode.direct_point0;
        bzero(bufferblock, BLOCK_SIZE);
        sbi_sd_read((unsigned int)bufferblock, 
            BLOCK_SECTOR_NUM, block_id);

        found = 0;
        for (int i = 0; i < temp_inode.dentry_num; i++)
        {
            memcpy((uint8_t *)&temp_dentry, (uint8_t *)(bufferblock + i * DENTRY_SIZE), DENTRY_SIZE);
            if (strcmp(temp_dentry.name, dirv[j]) == 0)
            {
                found = 1;
                break;
            }
        }
        if (!found)
        {
            prints("ERROR!!! path dir not found!!!\r");
            return;
        }
        get_inode(&temp_inode, temp_dentry.ino);
        if (temp_inode.mode != DIR_MODE)
        {
            prints("ERROR!!! path not a dir!!!\r");        
            return;
        }
    }

    // operate the file
    block_id = temp_inode.direct_point0;
    bzero(bufferblock, BLOCK_SIZE);
    sbi_sd_read((unsigned int)bufferblock, 
        BLOCK_SECTOR_NUM, block_id);
    found = 0;
    for (int i = 0; i < temp_inode.dentry_num; i++)
    {
        memcpy((uint8_t *)&temp_dentry, 
            (uint8_t *)(bufferblock + i * DENTRY_SIZE), DENTRY_SIZE);
        if (strcmp(temp_dentry.name, name) == 0)
        {
            found = 1;
            break;
        }
    }

    if (found)
    {
        get_inode(&temp_inode, temp_dentry.ino);
        if (temp_inode.mode != FILE_MODE && temp_inode.mode != SOFT_LINK_MODE)
        {
            prints("ERROR!!! not a file!!!\r");        
            return;
        }

        // soft link
        if (temp_inode.mode == SOFT_LINK_MODE)
        {
            bzero(bufferblock, BLOCK_SIZE);
            sbi_sd_read((unsigned int)bufferblock, 
                BLOCK_SECTOR_NUM, temp_inode.direct_point0);
            do_cat(bufferblock);
            return;
        }
        
        uint64_t byte_size = temp_inode.byte_size;
        int delta;
        for (int i = 0; i < temp_inode.block_size; i++)
        {
            delta = byte_size > BLOCK_SIZE ? BLOCK_SIZE : byte_size;
            print_content(&temp_inode, i, delta);
            byte_size -= delta;
        }
        prints("\rSUCCESS!!! print end!\r");
    }
    else
        prints("ERROR!!! file not exists!!!\r");
}

int do_fopen(char *name, int permission)
{
    int block_id = cur.direct_point0;
    bzero(bufferblock, BLOCK_SIZE);
    sbi_sd_read((unsigned int)bufferblock,BLOCK_SECTOR_NUM, block_id);

    int found = 0;
    dentry_t temp_dentry;
    for (int i = 0; i < cur.dentry_num; i++)
    {
        memcpy((uint8_t *)&temp_dentry, (uint8_t *)(bufferblock + i * DENTRY_SIZE), DENTRY_SIZE);
        if (strcmp(temp_dentry.name, name) == 0)//match name to find the file
        {
            found = 1;
            break;
        }
    }
    if (found)
    {
        inode_t temp_inode;
        get_inode(&temp_inode, temp_dentry.ino);//get the inode of the file
        if (temp_inode.mode != FILE_MODE)
        {
            prints("ERROR!!! not a file!!!\r");        
            return -1;
        }

        int j;
        for (int i = 0; i < BUCKET_NUM; i++)
            if (fd_open[i].valid == 0)
            {
                j = i;
                break;
            }

        fd_open[j].valid = 1;
        fd_open[j].ino = temp_dentry.ino;
        fd_open[j].permission = permission;
        fd_open[j].read_point = 0;
        fd_open[j].write_point = 0;

        return j;
    }
    else
    {
        prints("ERROR!!! file not exists!!!\r");
        return -1;
    }
}

void do_fwrite(int fd, char *buff, int size)
{
    if (!fd_open[fd].valid )
    {
        prints("ERROR!!! file not opened yet!\r");
        return ;
    }
    if (fd_open[fd].permission != O_WRONLY && fd_open[fd].permission != O_RDWR)
    {
        prints("ERROR!!! permission denied!!!\r");
        return ;
    }
    //see if can be written
    inode_t temp_inode;
    get_inode(&temp_inode, fd_open[fd].ino);//get the inode of the file

    int block_num = fd_open[fd].write_point / BLOCK_SIZE;
    int begin_point = fd_open[fd].write_point % BLOCK_SIZE;//calculate the block and offset
    int rest = BLOCK_SIZE - begin_point;//rest in the last block
    if (size <= rest)
    {
        write_content(&temp_inode, block_num, begin_point, size, buff);
        fd_open[fd].write_point += size;
        return ;
    }
    else
    {
        int total = 0;
        write_content(&temp_inode, block_num, begin_point, rest, buff);
        fd_open[fd].write_point += rest;
        total += rest;
        size -= rest;
        buff+=rest;
        block_num++;//write the rest of the block

        int len;//one time write size
        while (size)
        {
            len = size > BLOCK_SIZE ? BLOCK_SIZE : size;
            write_content(&temp_inode, block_num, 0, len, buff);
            fd_open[fd].write_point += len;
            total += len;
            size -= len;
            buff+=len;
            block_num +=len / BLOCK_SIZE;
        }//write the rest of the required
        return ;
    }
}

void do_fread(int fd, char *buff, int size)
{
//similar to fwrite
    if (fd_open[fd].valid == 0)
    {
        prints("ERROR!!! file not open!!!\r");
        return ;
    }
    if (fd_open[fd].permission != O_RDONLY && fd_open[fd].permission != O_RDWR)
    {
        prints("ERROR!!! permission denied!!!\r");
        return ;
    }
    
    inode_t temp_inode;
    get_inode(&temp_inode, fd_open[fd].ino);

    if (fd_open[fd].read_point >= temp_inode.byte_size)
    {
        return ;
    }
    
    int len;
    int block_num = fd_open[fd].read_point / BLOCK_SIZE;
    int begin_point = fd_open[fd].read_point % BLOCK_SIZE;
    int rest = BLOCK_SIZE - begin_point;
    if (size <= rest)
    {
        read_content(&temp_inode, block_num, begin_point, size, buff);
        fd_open[fd].read_point += size;
        return ;
    }
    else
    {
        int total = 0;
        read_content(&temp_inode, block_num, begin_point, rest, buff);
        fd_open[fd].read_point += rest;
        total += rest;
        size -= rest;
        buff+=rest;
        block_num++;//write the rest of the block

        int len;//one time write size
        while (size)
        {
            len = size > BLOCK_SIZE ? BLOCK_SIZE : size;
            read_content(&temp_inode, block_num, 0, len, buff);
            fd_open[fd].read_point += len;
            total += len;
            size -= len;
            buff+=len;
            block_num +=len / BLOCK_SIZE;
        }//write the rest of the required
        return ;
    }
}

void do_fclose(int fd)
{
    fd_open[fd].valid = 0;
}

