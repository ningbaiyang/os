#include <os/list.h>
//#include <os/mm.h>
#include <os/lock.h>
#include <os/sched.h>
#include <os/time.h>
#include <os/irq.h>
#include <screen.h>
#include <stdio.h>
#include <assert.h>
#include<sys/binsem.h>
#include <mailbox.h>
#include <string.h>*/
#include <common.h>
//#include <os/irq.h>
#include <os/mm.h>
//#include <os/sched.h>
#include <screen.h>
#include <sbi.h>
//#include <stdio.h>
//#include <os/time.h>
#include <os/syscall.h>
#include <os/futex.h>
#include <test.h>
//#include <sys/binsem.h>
#include <csr.h>
//#include <mailbox.h>


extern void ret_from_exception();
extern void printk_task1(void);
extern void __global_pointer$();
extern pid_t process_pid;
 void screen_write_ch(char ch);


pcb_t pcb[NUM_MAX_TASK];
extern int pcb_valid[100]={0};

const ptr_t pid0_stack = INIT_KERNEL_STACK + PAGE_SIZE;
pcb_t pid0_pcb = {
    .pid = 0,
    .kernel_sp = (ptr_t)pid0_stack,
    .user_sp = (ptr_t)pid0_stack,
    .preempt_count = 0
};
static void init_pcb_stack(
    ptr_t kernel_stack, ptr_t user_stack, ptr_t entry_point,
    pcb_t *pcb,void* argument)
{
    regs_context_t *pt_regs =
        (regs_context_t *)(kernel_stack - sizeof(regs_context_t));
    
    /* TODO: initialization registers
     * note: sp, gp, ra, sepc, sstatus
     * gp should be __global_pointer$
     * To run the task in user mode,
     * you should set corresponding bits of sstatus(SPP, SPIE, etc.).
     */
     for(int i=5;i<32;i++)
       pt_regs->regs[i]=0;
     pt_regs->regs[0]=0;
     pt_regs->regs[1]=entry_point;
     pt_regs->regs[2]=user_stack;
     pt_regs->regs[3]=__global_pointer$;
     pt_regs->regs[4]=(uint64_t)pcb;
     pt_regs->sstatus=0x20;
     pt_regs->sepc=entry_point;
     pt_regs->sbadaddr=0;
     pt_regs->scause=0;    
     pt_regs->regs[10]=(int)argument;

    // set sp to simulate return from switch_to
    /* TODO: you should prepare a stack, and push some values to
     * simulate a pcb context.
     */
    switchto_context_t *switchto_regs =
    	(switchto_context_t *)(kernel_stack - sizeof(regs_context_t)-sizeof(switchto_context_t));
    switchto_regs->regs[0] = ret_from_exception;
    switchto_regs->regs[1] = user_stack;
    for(int i=2;i<14;i++)
       switchto_regs->regs[i]=0;
}

 LIST_HEAD(ready_queue);
 LIST_HEAD(sleep_queue);
 LIST_HEAD(wait_queue);

/* current running task PCB */
pcb_t * volatile current_running;
pcb_t * volatile current_running_0;//master core
pcb_t * volatile current_running_1;//slave core 
pcb_t * volatile pre_running;
/* global process id */
pid_t process_pid = 1;


//bin binsignals[100];
void do_scheduler(void){
    // TODO schedule
    // Modify the current_running pointer.
    timer_check();
    pre_running=current_running;
	  current_running = list_entry(ready_queue.next, pcb_t, list);//pop(&ready_queue)-3*sizeof(reg_t);
    //if(pre_running!=&pid0_pcb)
      //list_add_tail(&(pre_running->list), &ready_queue);
    list_del(ready_queue.next);
    if(pre_running->status!=TASK_BLOCKED&&pre_running->status!=TASK_EXITED)
    {
      pre_running->status=TASK_READY;
      if(pre_running!=&pid0_pcb&&pre_running->status!=TASK_EXITED){
        list_add_tail(&(pre_running->list), &ready_queue);
      }
    }
    // restore the current_runnint's cursor_x and cursor_y
    vt100_move_cursor(current_running->cursor_x,
                      current_running->cursor_y);
    screen_cursor_x = current_running->cursor_x;
    screen_cursor_y = current_running->cursor_y;
    // TODO: switch_to current_running
    switch_to(pre_running,current_running);
}

void do_sleep(uint32_t sleep_time){
    // TODO: sleep(seconds)
    // note: you can assume: 1 second = `timebase` ticks
    // 1. block the current_running
    list_del(&(current_running->list));
    list_add_tail(&(current_running->list), &sleep_queue);
    current_running->status=TASK_BLOCKED;
    // 2. create a timer which calls `do_unblock` when timeout
    //timer_create(do_unblock, &(current_running->list), sleep_time);
    // 3. reschedule because the current_running is blocked.
    timer_create(&do_unblock,&current_running->list,(uint64_t)sleep_time);
    do_scheduler();
}

void do_block(list_node_t *pcb_node, list_head *queue){
    // TODO: block the pcb task into the block queue
  
  list_del(pcb_node);
	list_add_tail(pcb_node, queue);
	//*(sizeof(pid_t)+sizeof(task_type_t)+pcb_node)=TASK_BLOCKED; 
  (list_entry(pcb_node,pcb_t,list))->status=TASK_BLOCKED;
  if(ready_queue.next!=&ready_queue){
  do_scheduler();
  }
}

void do_unblock(list_node_t *pcb_node){
    // TODO: unblock the `pcb` from the block queue
    list_del(pcb_node);
    list_add_tail(pcb_node, &ready_queue);
    //*(sizeof(task_type_t)+sizeof(pid_t)+sizeof(list_node_t)+pcb_node)=TASK_READY;
    (list_entry(pcb_node,pcb_t,list))->status=TASK_READY;
}
pid_t do_spawn(task_info_t *info, void* arg, spawn_mode_t mode){
  //static void init_pcb_stack(ptr_t kernel_stack, ptr_t user_stack, ptr_t entry_point,pcb_t *pcb,void* argument);
  
	if(process_pid>=NUM_MAX_TASK)
		return process_pid;
  int i=0;
  for(i=0;i<100;i++){
    if(pcb_valid[i]==0){
      pcb_valid[i]=1;
     // pcb[i]=thread;
      break;
    }  
  }
	//pcb_t thread;
  pcb[i].pid=++process_pid;
 
	//pcb[thread.pid]=thread;
	//pcb_valid[thread.pid]=1;
	//if(arg!=NULL)
		//*info=*arg;
    pcb[i].kernel_sp = allocPage(1)+PAGE_SIZE;
    pcb[i].user_sp = allocPage(1)+PAGE_SIZE;
    init_pcb_stack(pcb[i].kernel_sp,pcb[i].user_sp,info->entry_point,&pcb[i],arg);
    list_add_tail(&(pcb[i].list),&ready_queue);
	  pcb[i].mode=mode;
    pcb[i].waitpid=-1;
  //  pcb[process_pid]=thread;
    pcb[i].type=info->type; 
    pcb[i].status=TASK_READY;  
    //do_scheduler();
    return pcb[i].pid;
}
void do_exit(void){
	 current_running->status=TASK_EXITED;
   list_del(&(current_running->list));
   int i;
   for(i=0;i<100;i++){
    if(pcb[i].pid==current_running->pid){
      pcb_valid[i] = 0;
      break;
    } 
   }
    //do_mutex_lock_release(current_running.mutex_lock);
    //if(current_running->mode==ENTER_ZOMBIE_ON_EXIT)
    	//current_running->status=TASK_ZOMBIE;
   for(i=0;i<100;i++){
    if(pcb[i].waitpid==current_running->pid){
      pcb[i].status=TASK_READY;
      list_add_tail(&(pcb[i].list),&ready_queue);
      break;
    } 
   }   
    do_scheduler();
}
pid_t do_getpid(){
    return current_running->pid;
}
int do_kill(pid_t pid){
  int i;
  for(i=0;i<100;i++){
    if(pcb[i].pid==pid){
      if(pcb_valid[i] == 0)
        return 0;
      pcb[i].status=TASK_EXITED;
      pcb_valid[i]=0;
      //pcb[i]=thread;
      break;
    }  
  }  
	/*if(pcb_valid[pid]==0)
		return 0;
    int i;
    for(i = 0; i < NUM_MAX_TASK; i++)
    {
        if(pcb[i].pid == pid)
            break;
    }
    pcb[i].status=TASK_EXITED;*/
    list_del(&(pcb[i].list));
    do_mutex_lock_release(pcb[i].mutex_lock);
    //pcb_valid[pid]=0;
    for(i=0;i<100;i++){
    if(pcb[i].waitpid==current_running->pid){
      pcb[i].status=TASK_READY;
      list_add_tail(&(pcb[i].list),&ready_queue);
      break;
    } 
   } 
    return 1;
}
int do_wait(pid_t pid){

/*	if(pcb_valid[pid]==0)
		return 0;
    int i;
    for(i = 0; i < NUM_MAX_TASK; i++)
    {
        if(pcb[i].pid == pid)
            break;
    }*/
    current_running->status=TASK_BLOCKED;
    list_del(&(current_running->list));
    list_add_tail(&(current_running->list),&wait_queue);
    current_running->waitpid=pid;
    do_scheduler();
    //while(pcb[pid]->status!=TASK_ZOMBIE&&pcb[pid]->status!=TASK_EXITED);
   // if(pcb[pid]->status==TASK_ZOMBIE)
    	//recycle(pid);//mm.c
}
void do_ps(uint32_t time){
	for(int i=0;i<NUM_MAX_TASK;i++)
	 if(pcb_valid[i]!=0){
	 	prints("[%d] ",i);
	 	prints("PID : %d",pcb[i].pid);
	 	prints("  STATUS ��%s:",pcb[i].status);
	 }

}

int binsemget(int key){
  for(int i=0;i<100;i++)
    if(binsignals[i].key==key)
      return i;
  return 0;
}
int binsemop(int binsem_id, int op){
  if(op==BINSEM_OP_LOCK){
    if(binsignals[binsem_id].status==BIN_LOCKED){
     do_block(&(current_running->list), &(binsignals[binsem_id].list));
     //binsignals[binsem_id].status=BIN_LOCKED;
    }
     binsignals[binsem_id].status=BIN_LOCKED;
  }
  else{
    if(!list_empty(&binsignals[binsem_id].list)){
    do_unblock(binsignals[binsem_id].list.next);
    binsignals[binsem_id].status=BIN_LOCKED;
    }
    else binsignals[binsem_id].status=BIN_UNLOCKED;
    //do_scheduler();
  }
}
/*
mailbox_t do_mbox_open(char *name)
{
    // TODO:
    
    for(int i=0;i<mailbox_num;i++){
    	if(strcmp(name, boxes[i]->name)==0){
    		boxes[i].user++;
    		return boxes[i];
    	}
	}
	mailbox_t mailbox;
	mthread_mutex_init(&(mailbox.mutex));
	mthread_cond_init(&(mailbox.full));
	mthread_cond_init(&(mailbox.empty));
	strcpy(mailbox.name, name);
	mailbox.front=0;
	mailbox.rear=0;
	mailbox.wait=0;
	mailbox.user=1;
	mailbox.message='\0';
	mailbox.left_space=MAX_MBOX_LENGTH;
	boxes[mailbox_num++]=mailbox;
	return mailbox;
	//mailbox.
}

void do_mbox_close(mailbox_t mailbox)
{
    // TODO:
    mailbox.user--;
    if(mailbox.user<=0){
    	mthread_cond_broadcast(&(mailbox.full));
    	mthread_cond_broadcast(&(mailbox.empty));
    	mthread_mutex_unlock(&(mailbox.mutex));
 		mthread_mutex_init(&(mailbox.mutex));
		mthread_cond_init(&(mailbox.full));
		mthread_cond_init(&(mailbox.empty));
		mailbox.front=0;
		mailbox.rear=0;
		mailbox.wait=0;
		mailbox.user=0;   
		mailbox.message='\0';	
		mailbox.left_space=MAX_MBOX_LENGTH;
	}
}

void do_mbox_send(mailbox_t mailbox, void *msg, int msg_length)
{
    // TODO:
    mthread_mutex_lock(&(mailbox.mutex));
    while(mailbox.left_space < msg_length)
        mthread_cond_wait(&(mailbox.full), &(mailbox.mutex));
    if(MAX_MBOX_LENGTH - mailbox.rear < msg_length)
    {
        memcpy(mailbox.msg + mailbox.rear, msg, MAX_MBOX_LENGTH - mailbox.rear);
        mailbox.rear = (msg_length + mailbox.rear) % MAX_MBOX_LENGTH;
        memcpy(mailbox.msg, msg + msg_length - mailbox.rear, mailbox.rear);
    }
    else
    {
        memcpy(mailbox.msg + mailbox.rear,msg, msg_length);
        mailbox.rear += msg_length;
    }

    mailbox.left_space -= msg_length;
    do_mthread_cond_broadcast(&(mailbox.empty));
    mthread_mutex_lock(&(mailbox.mutex));
}
void do_mbox_recv(mailbox_t mailbox, void *msg, int msg_length)
{
    // TODO:
    mthread_mutex_lock(&(mailbox.mutex));
    while(MAX_MBOX_LENGTH - mailbox.left_space < msg_length)
        mthread_cond_wait(&(mailbox.empty), &(mailbox.mutex));
    if(MAX_MBOX_LENGTH - mailbox.front < msg_length)
    {
        memcpy(msg,mailbox.msg + mailbox.front, MAX_MBOX_LENGTH - mailbox.front);
        mailbox.front = (msg_length + mailbox.front) % MAX_MBOX_LENGTH;
        memcpy(msg + msg_length - mailbox.front, mailbox.msg, mailbox.front);
    }
    else
    {
        memcpy(msg, mailbox.msg + mailbox.front, msg_length);
        mailbox.front += msg_length;
    }
    mailbox.left_space += msg_length;
    do_mthread_cond_broadcast(&(mailbox.full));
    mthread_mutex_lock(&(mailbox.mutex));
}
*/
int do_mthread_barrier_init(mthread_barrier_t * barrier, unsigned count)
{
    // TODO:
    barrier->expect=count;
    barrier->num=0;
    init_list_head(&(barrier->barrier_queue));
    return 0;
}
int do_mthread_barrier_wait(mthread_barrier_t *barrier)
{
    // TODO:
    barrier->num+=1;
    if(barrier->num==barrier->expect){
    	while(!(barrier->barrier_queue.next==&(barrier->barrier_queue)))
    		do_unblock( barrier->barrier_queue.next);
    		barrier->num=0;
    	//do_scheduler(); 
	}
	else
		do_block(&(current_running->list),&(barrier->barrier_queue));
	return 0;
}
int do_mthread_barrier_destroy(mthread_barrier_t *barrier)
{
    // TODO:
    while(!(barrier->barrier_queue.next==&(barrier->barrier_queue)))
    	do_unblock( barrier->barrier_queue.next);
    //free(barrier);
    return 0;
}

int do_mthread_cond_init(mthread_cond_t *cond)
{
    // TODO:
    cond->num=0;
    init_list_head(&(cond->cond_queue));
    return 0;
}
int do_mthread_cond_destroy(mthread_cond_t *cond) {
    // TODO:
    //do_mthread_cond_broadcast(cond);
    //free(cond);
    cond->num=0;
    init_list_head(&(cond->cond_queue));
    return 0;
}
int do_mthread_cond_wait(mthread_cond_t *cond, mthread_mutex_t *mutex)
{
    // TODO:
    if(mutex->status==EBUSY){
    do_mthread_mutex_unlock(mutex);
    do_block(&(current_running->list),&(cond->cond_queue));
    cond->num++;
    }
    //do_mthread_mutex_trylock(mutex);
    return 0;
}
int do_mthread_cond_signal(mthread_cond_t *cond)
{
    // TODO:
    do_unblock(cond->cond_queue.next);
    cond->num--;
}
int do_mthread_cond_broadcast(mthread_cond_t *cond)
{
    // TODO:
   while(!list_empty(&cond->cond_queue)){
    	do_unblock( cond->cond_queue.next);
   cond->num--;
   }
}
int do_mthread_mutex_init(mthread_mutex_t *lock)
{
    lock->status=AVAILABLE;
    init_list_head(&(lock->block_queue));
    return 1;
}
int do_mthread_mutex_destroy(mthread_mutex_t *lock) {
    // TODO:
    //while(lock->block_queue.next=&(lock->block_queue))
    //	do_unblock(lock->block_queue.next);
    lock->status=DESTROYED;
    return 1;
    //free(lock);
}
int do_mthread_mutex_trylock(mthread_mutex_t *lock) {
    /* TODO */
    if(lock->status==AVAILABLE){
    	//list_add_tail(&(current_running->list), &ready_queue);
    	lock->status=EINVAL;
    	current_running->mutex_lock=lock;
    	return 1;

  	}
    	
    else{
       return 0;
	 }
}
int do_mthread_mutex_lock(mthread_mutex_t *lock) {
    // TODO:
    if(lock->status==AVAILABLE){
    	//list_add_tail(&(current_running->list), &ready_queue);
    	lock->status=EBUSY;
    	current_running->mutex_lock=lock;

  	}
    	
    else{
      do_block(&(current_running->list),&(lock->block_queue));
    	//list_add_tail(&(current_running->list), &(lock->block_queue));
	 }
   return 1;
}
int do_mthread_mutex_unlock(mthread_mutex_t *lock)
{
    list_entry(lock,pcb_t,mutex_lock)->mutex_lock=NULL;
    if(lock->block_queue.next!=&(lock->block_queue)){
      do_unblock(lock->block_queue.next);
      lock->status=EBUSY;
    }
    else{
      lock->status=AVAILABLE;
    }
    return 1;
}