/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * *
 *            Copyright (C) 2018 Institute of Computing Technology, CAS
 *               Author : Han Shukai (email : hanshukai@ict.ac.cn)
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * *
 *                  The shell acts as a task running in user mode.
 *       The main function is to make system calls through the user's output.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * */

#include <test.h>
#include <string.h>
#include <os.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <stdint.h>
#include <screen.h>

struct task_info task_test_waitpid = {
    (uintptr_t)&wait_exit_task, USER_PROCESS};
struct task_info task_test_semaphore = {
    (uintptr_t)&semaphore_add_task1, USER_PROCESS};
struct task_info task_test_condition = {
    (uintptr_t)&test_condition, USER_PROCESS};
struct task_info task_test_barrier = {
    (uintptr_t)&test_barrier, USER_PROCESS};
struct task_info task_test_fs = {
    (uintptr_t)&test_fs, USER_PROCESS};

struct task_info task13 = {(uintptr_t)&SunQuan, USER_PROCESS};
struct task_info task14 = {(uintptr_t)&LiuBei, USER_PROCESS};
struct task_info task15 = {(uintptr_t)&CaoCao, USER_PROCESS};
struct task_info task_test_multicore = {(uintptr_t)&test_multicore, USER_PROCESS};

static struct task_info *test_tasks[16] = {&task_test_fs,
                                           &task_test_semaphore,
                                           &task_test_condition,
                                           &task_test_barrier,
                                           &task13, &task14, &task15,
                                           &task_test_multicore};
static int num_test_tasks = 8;

#define SHELL_BEGIN 25
#define LEN 10000

int getchar_shell(){
	while(1){
		int c=sys_get_char();
		if(c!=-1){
			return c;
	    }
	}
}
void putchar_shell(char in){
	sys_screen_write_ch(in);
}
void read_port(char buffer[]){
	int in;
	int i=0;
	int num=0;
	while((in=getchar_shell())!=13){
		//num++;
		//if(!(in==8||in==127))
			//putchar_shell(in);
      //printf("%c",in);
		if(in!=8&&in!=127){
			num++;
			char c=in;
			buffer[i++]=c;
      printf("%c",in);
		}
		else{
			if(i>0)
				i--;
			if(num>0) 
				num--;
        printf("%c",in);
		}
		//if(!(num>0))
		//	putchar_shell(in);

	}
  printf("\n");
	buffer[i]='\0';
	//putchar();
}
int test_memcmp(const char *str1, const char *str2,uint32_t num){
    while (*str1 && *str2&&num) {
        if (*str1 != *str2) {
            return (*str1) - (*str2);
        }
        ++str1;
        ++str2;
        num--;
    }
    //return (*str1) - (*str2);
    return 0;	
}
int parse_input(char buffer1[]){
  int l;
	int i=0;
	int j=0;
	char buffer[LEN];
	while(buffer1[i]!='\0'){
		if(buffer1[i]!=' ')
			buffer[j++]=buffer1[i];
		i++;
	} //ȥ���ո� 
  buffer[j]='\0';
  char _arg_buf[50] = {0};
	if(buffer[0]=='k'&&buffer[1]=='i'&&buffer[2]=='l'&&buffer[3]=='l')//kill
		sys_kill((int)(buffer[4]));
	else if(buffer[0]=='w'&&buffer[1]=='a'&&buffer[2]=='i'&&buffer[3]=='t')//wait 
		sys_waitpid((int)(buffer[4]));
	else if(buffer[0]=='e'&&buffer[1]=='x'&&buffer[2]=='i'&&buffer[3]=='t')//exit
		sys_exit();
	else if(buffer[0]=='p'&&buffer[1]=='s')//ps
		sys_ps();
	else if(buffer[0]=='c'&&buffer[1]=='l'&&buffer[2]=='e'&&buffer[3]=='a'&&buffer[4]=='r'){//clear
		sys_clear();
    sys_move_cursor(1, 25);
    printf("------------------- COMMAND -------------------\n");}
    else if (test_memcmp(buffer, "mkfs", 4) == 0)
    {
        sys_mkfs();
    }
    else if (test_memcmp(buffer, "statfs", 6) == 0)
    {
        sys_fs_info();
    }
    else if (test_memcmp(buffer, "cd", 2) == 0)
    {
        strcpy(_arg_buf, &buffer[2]);
        sys_enter_fs(_arg_buf);
    }
    // make dir
    else if (test_memcmp(buffer, "mkdir", 5) == 0)
    {
        strcpy(_arg_buf, &buffer[5]);
        sys_mkdir(_arg_buf);
    }
    // remove dir
    else if (test_memcmp(buffer, "rmdir", 5) == 0)
    {
        strcpy(_arg_buf, &buffer[5]);
        sys_rmdir(_arg_buf);
    }
    // print content in dir
    else if (test_memcmp(buffer, "ls", 2) == 0)
    {
        l = strlen(buffer);
        if(l > 2){
            strcpy(_arg_buf, &buffer[2]);
        }else{
            _arg_buf[0]='\0';
    	}
            sys_read_dir(_arg_buf);
    }
    // create a file
    else if (test_memcmp(buffer, "touch", 5) == 0)
    {
        strcpy(_arg_buf, &buffer[5]);
        sys_mknod(_arg_buf);
    }
           // print content in file
    else if (test_memcmp(buffer, "cat", 3) == 0)
    {
        strcpy(_arg_buf, &buffer[3]);
        sys_cat(_arg_buf);
    }

	else if(buffer[0]=='e'&&buffer[1]=='x'&&buffer[2]=='e'&&buffer[3]=='c'){//exec
			//task_info* task=test_tasks[ atoi(buffer[4])   ];
      int a=buffer[4]-'0' ;
            printf("%d",a);
            sys_spawn(test_tasks[ buffer[4]-'0'   ],NULL,0);
   }
	else
		printf("Unknown command");
}
void test_shell()
{
    // TODO:
    sys_move_cursor(1, SHELL_BEGIN);
    printf("------------------- COMMAND -------------------\n");
    printf("> root@UCAS_OS: ");
    sys_reflush();
    while (1)
    {
        // TODO: call syscall to read UART port
        char buffer[LEN];
        read_port(buffer);
        //printf("%s\n",buffer);
        // TODO: parse input
        parse_input(buffer);
        // note: backspace maybe 8('\b') or 127(delete)
        // TODO: ps, exec, kill, clear
       // execute_command(buffer,ins);
    	printf("\n> root@UCAS_OS: ");
      sys_reflush();
    }
}