#include <mailbox.h>
#include <string.h>
#include <mthread.h>
int mailbox_num=0;
mailbox_t mbox_open(char *name)
{
    // TODO:
    int i=0;
  for(i=0;i<mailbox_num;i++){
    	if(strcmp(name, boxes[i].name)==0){
    		boxes[i].user++;
    		return i;
    	}
	}
	mbox mailbox;
	mthread_mutex_init(&(mailbox.mutex));
	mthread_cond_init(&(mailbox.full));
	mthread_cond_init(&(mailbox.empty));
	strcpy(mailbox.name, name);
	mailbox.front=0;
	mailbox.rear=0;
	mailbox.wait=0;
	mailbox.user=1;
	mailbox.msg[0]='\0';
	mailbox.left_space=MAX_MBOX_LENGTH;
	boxes[mailbox_num++]=mailbox;
	return i;
}

void mbox_close(mailbox_t mailbox)
{
    // TODO:
    boxes[mailbox].user--;
    if(boxes[mailbox].user<=0){
    	mthread_cond_broadcast(&(boxes[mailbox].full));
    	mthread_cond_broadcast(&(boxes[mailbox].empty));
    	mthread_mutex_unlock(&(boxes[mailbox].mutex));
 		  mthread_mutex_init(&(boxes[mailbox].mutex));
		  mthread_cond_init(&(boxes[mailbox].full));
		  mthread_cond_init(&(boxes[mailbox].empty));
		  boxes[mailbox].front=0;
		  boxes[mailbox].rear=0;
		  boxes[mailbox].wait=0;
		  boxes[mailbox].user=0;   
		  boxes[mailbox].msg[0]='\0';	
		  boxes[mailbox].left_space=MAX_MBOX_LENGTH;
	 }
}

void mbox_send(mailbox_t mailbox, void *msg, int msg_length)
{
    // TODO:
    mthread_mutex_lock(&(boxes[mailbox].mutex));
    while(boxes[mailbox].left_space < msg_length)
        mthread_cond_wait(&(boxes[mailbox].full), &(boxes[mailbox].mutex));
    if(MAX_MBOX_LENGTH - boxes[mailbox].rear < msg_length)
    {
        memcpy(boxes[mailbox].msg + boxes[mailbox].rear, msg, MAX_MBOX_LENGTH - boxes[mailbox].rear);
        boxes[mailbox].rear = (msg_length + boxes[mailbox].rear) % MAX_MBOX_LENGTH;
        memcpy(boxes[mailbox].msg, msg + msg_length - boxes[mailbox].rear, boxes[mailbox].rear);
    }
    else
    {
        memcpy(boxes[mailbox].msg + boxes[mailbox].rear,msg, msg_length);
        boxes[mailbox].rear += msg_length;
    }

    boxes[mailbox].left_space -= msg_length;
    do_mthread_cond_broadcast(&(boxes[mailbox].empty));
    mthread_mutex_lock(&(boxes[mailbox].mutex));
}
void mbox_recv(mailbox_t mailbox, void *msg, int msg_length)
{
    // TODO:
    mthread_mutex_lock(&(boxes[mailbox].mutex));
    while(MAX_MBOX_LENGTH - boxes[mailbox].left_space < msg_length)
        mthread_cond_wait(&(boxes[mailbox].empty), &(boxes[mailbox].mutex));
    if(MAX_MBOX_LENGTH - boxes[mailbox].front < msg_length)
    {
        memcpy(msg,boxes[mailbox].msg + boxes[mailbox].front, MAX_MBOX_LENGTH - boxes[mailbox].front);
        boxes[mailbox].front = (msg_length + boxes[mailbox].front) % MAX_MBOX_LENGTH;
        memcpy(msg + msg_length - boxes[mailbox].front, boxes[mailbox].msg, boxes[mailbox].front);
    }
    else
    {
        memcpy(msg, boxes[mailbox].msg + boxes[mailbox].front, msg_length);
        boxes[mailbox].front += msg_length;
    }
    boxes[mailbox].left_space += msg_length;
    do_mthread_cond_broadcast(&(boxes[mailbox].full));
    mthread_mutex_lock(&(boxes[mailbox].mutex));
}
